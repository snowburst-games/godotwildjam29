using Godot;
using System;

public class LevelScene : Node2D
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	[Export]
	public StageWorld.Level NextLevel {get; set;}

	[Export]
	public string HumanReadableName {get; set;} = "";

	[Export]
	public string EndLevelText {get; set;} = "";

	[Export]
	public AudioData.WorldSounds EndLevelNarration {get; set;}

	[Export]
	public string EndLevelPicturePath {get; set;}

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
