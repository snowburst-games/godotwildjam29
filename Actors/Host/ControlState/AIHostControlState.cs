using Godot;
using System;

public class AIHostControlState : HostControlState
{

	public HostAIState HostAIState {get; set;}
	public enum AIState {Idle, Suspecting, Caught, Infested, Fleeing}

	public AIHostControlState()
	{
		
	}
	public AIHostControlState(Host host)
	{
		this.Host = host;
		SetAIState(AIState.Idle);
	}

	public override void Update(float delta)
	{
		base.Update(delta);

		if (HostAIState == null)
		{
			return;
		}

		HostAIState.Update(delta);
	}

	public override void SetAIState(AIState state)
	{
		if (HostAIState != null)
		{
			HostAIState.Exit();
		}
		switch (state)
		{
			case AIState.Idle:
				HostAIState = new IdleHostAIState(Host);
				break;
			case AIState.Suspecting:
				HostAIState = new SuspectingHostAIState(Host);
				break;
			case AIState.Infested:
				HostAIState = new InfestedHostAIState(Host);
				break;
			case AIState.Caught:
				HostAIState = new CaughtHostAIState(Host);
				break;
			case AIState.Fleeing:
				HostAIState = new FleeingHostAIState(Host);
				break;
		}
	}

	public AIState GetActionState()
	{
		if (HostAIState is IdleHostAIState)
		{
			return AIState.Idle;
		}
		else if (HostAIState is SuspectingHostAIState)
		{
			return AIState.Suspecting;
		}
		else if (HostAIState is InfestedHostAIState)
		{
			return AIState.Infested;
		}
		else if (HostAIState is CaughtHostAIState)
		{
			return AIState.Caught;
		}
		else
		{
			return AIState.Fleeing;
		}
	}

	public override void Exit()
	{
		base.Exit();
		if (HostAIState != null)
		{
			HostAIState.Exit();
		}
	}
}
