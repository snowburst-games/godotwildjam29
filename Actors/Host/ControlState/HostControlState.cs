using Godot;
using System;

public class HostControlState
{

	public Host Host {get; set;}
	public Vector2 TryMoveDir = new Vector2();

	public virtual void Update(float delta)
	{
		
	}

	public virtual void SetAIState(AIHostControlState.AIState state)
	{

	}

	public virtual void Exit()
	{
		
	}

}
