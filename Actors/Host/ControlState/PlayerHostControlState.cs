using Godot;
using System;

public class PlayerHostControlState : HostControlState
{

	public PlayerHostControlState()
	{
		
	}
	public PlayerHostControlState(Host host)
	{
		this.Host = host;
		Host.SpriteSuspectArea.Visible = false;

		if (Host.IsPerfectHost)
		{
			Host.PlayAnim("PerfectHost");
		}
		else if (Host.IsContaminated)
		{
			Host.OnContaminatedHostEntered();
			// Host.PlayAnim("ContaminatedHost");
		}
		Host.InfestTimer.Start();
	}

	public override void Update(float delta)
	{
		base.Update(delta);
		bool up = Input.IsActionPressed("ui_up");
		bool down =  Input.IsActionPressed("ui_down");
		bool left =  Input.IsActionPressed("ui_left");
		bool right =  Input.IsActionPressed("ui_right");
		bool stopInfesting = Input.IsActionPressed("Interact");
		
		if (Host.InfestTimer.TimeLeft > 0)
		{
			stopInfesting = false;
		}

		TryMoveDir = new Vector2( left ? -1 : (right ? 1 : 0), up ? -1 : (down ? 1 : 0));

		if (stopInfesting)
		{
			Host.LeaveHost();
		}

		// RotateForward();

		if (!Host.IsPerfectHost)
		{
			Host.Health -= Host.HEALTH_DRAIN * delta;
		}

		
	}

	// public void RotateForward()
	// {
	// 	if (TryMoveDir != new Vector2(0,0))
	// 	{
	// 		foreach (Node n in Host.GetChildren())
	// 		{
	// 			if (n is Node2D node2D)
	// 			{
	// 				node2D.LookAt(Host.Position + (Host.HostControlState.TryMoveDir.Normalized() * Host.SPEED));
	// 			}
	// 		}
	// 	}
	// }
}
