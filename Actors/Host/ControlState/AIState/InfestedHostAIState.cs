using Godot;
using System;

public class InfestedHostAIState : HostAIState
{
	public InfestedHostAIState()
	{

	}

	public InfestedHostAIState(Host host)
	{
		this.Host = host;
		Host.SpriteSuspectArea.Visible = false;
	}
}
