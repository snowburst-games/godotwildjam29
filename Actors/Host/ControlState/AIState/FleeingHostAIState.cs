using Godot;
using System;

public class FleeingHostAIState : HostAIState
{
	private Random _rand = new Random();
	public FleeingHostAIState()
	{

	}

	public FleeingHostAIState(Host host)
	{
		this.Host = host;

		Host.GetNode<Label>("LblAIState").Text = "I am afraid!";
		Host.SpriteSuspectArea.Visible = false;
		Host.Speed = Host.STARTING_SPEED * 1.25f;
		CurrentTarget = GetRandomTarget(true);// GetTargetNFromPosition(Host.FleeFromHere, 1000);
		Host.PlayVoice(GetRandomFleeParasiteSound());
	}
	// private bool _targetCalculated = false;
	private AudioData.HostSound GetRandomFleeParasiteSound()
	{
		return ((new AudioData.HostSound[3] {AudioData.HostSound.Flee1, AudioData.HostSound.Flee2, AudioData.HostSound.Flee3})[_rand.Next(3)]);
	}
	public override void Update(float delta)
	{
		base.Update(delta);

		// if (!_targetCalculated)
		// {
		// 	if (IsDetectRayCollidingParasite())
		// 	{
		// 		UpdateParasiteLocation();
		// 		CurrentTarget = GetOppositeTarget(LastKnownTarget, 20f);
		// 	}
		// 	_targetCalculated = true;
		// }
		// if (_targetCalculated)
		// {
			if (Host.Position.DistanceTo(CurrentTarget) > 100f)
			{
				CalculateVelocity();
				OrientAnimation();
				MoveCollide(delta);
			}
			else
			{
				Host.PlayAnimFrame("Idle");
				Host.HostControlState.SetAIState(AIHostControlState.AIState.Idle);
				
			}
		// }


	}

	public override void Exit()
	{
		Host.Speed = Host.STARTING_SPEED;
	}
}
