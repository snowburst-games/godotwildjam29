using Godot;
using System;

public class CaughtHostAIState : HostAIState
{
	private Random _rand = new Random();
	public CaughtHostAIState()
	{

	}
	public CaughtHostAIState(Host host)
	{
		this.Host = host;

		Host.GetNode<Label>("LblAIState").Text = "I have caught you!";
		Host.PlayAnim("Caught");
		Host.PlaySoundEffect(AudioData.HostSound.Fire);
		Host.PlayVoice(GetRandomCaughtSound());
		Host.SpriteSuspectArea.Visible = false;
	}

	public AudioData.HostSound GetRandomCaughtSound()
	{
		return ((new AudioData.HostSound[3] {AudioData.HostSound.CaughtParasite1, AudioData.HostSound.CaughtParasite2, AudioData.HostSound.CaughtParasite3})[_rand.Next(3)]);
	}
}
