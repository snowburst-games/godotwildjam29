using Godot;
using System;

public class SuspectingHostAIState : HostAIState
{
	public SuspectingHostAIState()
	{

	}

	private const float SUSPECT_LEVEL_DRAIN = 10f;
	private const float DETECT_MOVE_DISTANCE_BUFFER = 75f;

	private float _suspectLevel = 0f;
	private const float START_SUSPECT_LEVEL = 40f;
	private Random _rand = new Random();

	public SuspectingHostAIState(Host host)
	{
		this.Host = host;
		Host.GetNode<Label>("LblAIState").Text = "I am suspicious";
		Host.IsSuspicious = true;
		_suspectLevel = START_SUSPECT_LEVEL;
		Host.Speed = Host.STARTING_SPEED / 1.5f;
		Host.SpriteSuspectArea.Visible = true;
		Host.SuspicionBar.Visible = true;
		Host.EmitSignal(nameof(Host.BecameSuspicious), Host.Position);
		if (Host.NearbyHostSuspicious)
		{
			// _suspectLevel = START_SUSPECT_LEVEL;
			Host.NearbyHostSuspicious = false;
		}
		Host.PlayVoice(GetRandomSuspectingSound());
	}

	private AudioData.HostSound GetRandomSuspectingSound()
	{
		return ((new AudioData.HostSound[3] {AudioData.HostSound.Suspicious1, AudioData.HostSound.Suspicious2, AudioData.HostSound.Suspicious3})[_rand.Next(3)]);
	}
	private AudioData.HostSound GetRandomLostParasiteSound()
	{
		return ((new AudioData.HostSound[3] {AudioData.HostSound.LostParasite1, AudioData.HostSound.LostParasite2, AudioData.HostSound.LostParasite3})[_rand.Next(3)]);
	}

	public override void Update(float delta)
	{
		base.Update(delta);

		
		if (IsDetectRayCollidingParasite())
		{
			UpdateParasiteLocation();
			_suspectLevel += Host.Position.DistanceTo(CurrentTarget) < DETECT_MOVE_DISTANCE_BUFFER*1.25f ? (SUSPECT_LEVEL_DRAIN*2 * Host.StealthModifier) * delta : (SUSPECT_LEVEL_DRAIN  * Host.StealthModifier) * delta;
			_suspectLevel = Math.Min(100f, _suspectLevel);
		}
		else
		{
			_suspectLevel -= (SUSPECT_LEVEL_DRAIN / Host.StealthModifier) * delta;
			_suspectLevel = Math.Max(-10f, _suspectLevel);
		}
		
		Host.GetNode<ProgressBar>("SuspicionBar").AddStyleboxOverride("fg", ((StyleBoxFlat)GD.Load(("res://Actors/Host/Interface/BarForegroundW.tres")))); // Modulate = new Color(0,0,1);
		Host.GetNode<ProgressBar>("SuspicionBar").Value = Mathf.Clamp(_suspectLevel, 0, 100);
		
		CurrentTarget = LastKnownTarget;

		if (_suspectLevel < 5)
		{
			Host.PlayAnimFrame("Idle");
		}
		else if (_suspectLevel >= 5)
		{
			// for testing:
			// Host.HostControlState.SetAIState(AIHostControlState.AIState.Fleeing);
			// return;
			//
			Host.GetNode<ProgressBar>("SuspicionBar").AddStyleboxOverride("fg", ((StyleBoxFlat)GD.Load(("res://Actors/Host/Interface/BarForegroundY.tres"))));
			if (Host.Position.DistanceTo(CurrentTarget) > DETECT_MOVE_DISTANCE_BUFFER)
			{
				CalculateVelocity();
				OrientAnimation();
				MoveCollide(delta);
			}
			else
			{
				// if (!IsDetectRayCollidingParasite())
				// {
					// CurrentTarget = GetRandomTarget();
				// }
				// else
				// {
					Host.PlayAnimFrame("Idle");
				// }
			}
		}
		if (_suspectLevel >= 60f)
		{
			Host.GetNode<ProgressBar>("SuspicionBar").AddStyleboxOverride("fg", ((StyleBoxFlat)GD.Load(("res://Actors/Host/Interface/BarForegroundR.tres"))));
		}

		if (IsDetectRayCollidingParasite() && _suspectLevel >= 60f && Host.Position.DistanceTo(CurrentTarget) < DETECT_MOVE_DISTANCE_BUFFER*1f)
		{
			Host.HostControlState.SetAIState(AIHostControlState.AIState.Caught);
		}
		if (_suspectLevel <= 0f)
		{
			Host.PlayVoice(GetRandomLostParasiteSound());
			// Host.SuspicionBar.Visible = false;
			Host.HostControlState.SetAIState(AIHostControlState.AIState.Idle);
		}

	}

	public override void Exit()
	{
		base.Exit();

		Host.SuspicionBar.Visible = false;
		Host.Speed = Host.STARTING_SPEED;
	}
}
