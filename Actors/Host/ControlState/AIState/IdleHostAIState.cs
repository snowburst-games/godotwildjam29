using Godot;
using System;
// AI state - patrol to random targets from origin, change direction on obstacle and on x distance from origin (record the origin on ready).
// raycast? or area2d cone, and activate parasite OnSuspected every frame

public class IdleHostAIState : HostAIState
{
	private int[] _waitTimeRange = new int[2] {4,14};

	private bool _nextIdle = true;

	public IdleHostAIState()
	{

	}

	public IdleHostAIState(Host host)
	{
		this.Host = host;
		CurrentTarget = GetRandomTarget();
		Host.IsSuspicious = false;
		Host.GetNode<Label>("LblAIState").Text = "I am idle";
		Host.SpriteSuspectArea.Visible = true;
		Host.Speed = Host.STARTING_SPEED/2f;
	}

	public override void Update(float delta)
	{
		base.Update(delta);

		// when we reach our destination, idle for a few seconds, or if already idled, move to a new target
		if (Host.Position.DistanceTo(CurrentTarget) < DISTANCE_BUFFER*2)
		{
			if (_nextIdle)
			{
				if (Host.IdleAITimer.TimeLeft == 0)
				{
					StartIdleTimer();
					// Host.IdleAITimer.Stop(); /// sets time left to 0
					_nextIdle = false;
				}
			}
			else
			{
				CurrentTarget = GetRandomTarget();
				_nextIdle = true;
			}
			Host.PlayAnimFrame("Idle");
			Host.PlayVoice(AudioData.HostSound.IdleHum);
		}
		else
		{
			CalculateVelocity();
			OrientAnimation();
			MoveCollide(delta);
		}
		if (GetCollisionPoint() != new Vector2(-99999, -99999))
		{
			CurrentTarget = GetOppositeTarget(GetCollisionPoint());
		}

		if (IsDetectRayCollidingParasite())
		{
			Host.HostControlState.TryMoveDir = new Vector2(0,0);
			Host.HostControlState.SetAIState(AIHostControlState.AIState.Suspecting);
		}
	}	

	private bool IsDetectRayColliding()
	{
		foreach (RayCast2D ray in Host.DetectRays)
		{
			if (ray.IsColliding())
			{
				return true;
			}
		}
		return false;
	}

	private void StartIdleTimer()
	{
		Host.IdleAITimer.WaitTime = Rand.Next(_waitTimeRange[0],_waitTimeRange[1]);
		Host.IdleAITimer.Start();
	}


	public override void Exit()
	{
		Host.Speed = Host.STARTING_SPEED;
	}

}
