using Godot;
using System;

public class HostAIState
{
	public Host Host {get; set;}
	public Random Rand = new Random();
	public const float DISTANCE_BUFFER = 5f;
	public const int PATROL_DISTANCE = 300;
	public Vector2 CurrentTarget {get; set;}
	public Vector2 LastKnownTarget {get; set;}
	public Vector2 Velocity {get; set;}
	public float SteerForce = 0.55f; // higher = turn faster

	public virtual void Update(float delta)
	{
		
	}
	// public virtual void Init()
	// {

	// }	
	public bool IsDetectRayCollidingParasite()
	{
		foreach (RayCast2D ray in Host.DetectRays)
		{
			if (ray.IsColliding())
			{
				if (ray.GetCollider() is Area2D area)
				{
					if (area.Name == "DetectArea")
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	public virtual void Exit()
	{
		
	}

	public Vector2 GetCollisionPoint()
	{
		foreach (RayCast2D ray in Host.CollisionRays)
		{
			if (ray.IsColliding())
			{
				if (!(ray.GetCollider() is PhysicsBody2D || ray.GetCollider() is TileMap))
				{
					continue;
				}
				// GD.Print(ray.GetCollider());
				return ray.GetCollisionPoint();
			}
		}
		return new Vector2(-99999,-99999);
	}


	public void UpdateParasiteLocation()
	{
		
		foreach (RayCast2D ray in Host.DetectRays)
		{
			if (ray.IsColliding())
			{
				if (ray.GetCollider() is Area2D area)
				{
					if (area.Name == "DetectArea")
					{
						LastKnownTarget = area.GlobalPosition;
						return;
					}
				}
			}
		}
	}

	public Vector2 GetRandomTarget(bool flee = false)
	{
		int distance = PATROL_DISTANCE;
		if (flee)
		{
			distance *= 4;
		}
		return new Vector2(Host.StartPosition.x + Rand.Next(-distance,distance), Host.StartPosition.y + Rand.Next(-distance,distance));
	}

	public Vector2 GetOppositeTarget(Vector2 opposingPosition, float scale = 1f)
	{
		Vector2 oppositeTarget = Host.Position;
		if (opposingPosition.x > Host.Position.x)
		{
			oppositeTarget.x -= Rand.Next(PATROL_DISTANCE/10, PATROL_DISTANCE/6) * scale;
		}
		else
		{
			oppositeTarget .x +=  Rand.Next(PATROL_DISTANCE/10, PATROL_DISTANCE/6) * scale;
		}
		if (opposingPosition.y > Host.Position.y)
		{
			oppositeTarget.y -= Rand.Next(PATROL_DISTANCE/10, PATROL_DISTANCE/6) * scale;
		}
		else
		{
			oppositeTarget.y +=  Rand.Next(PATROL_DISTANCE/10, PATROL_DISTANCE/6) * scale;
		}
		

		return oppositeTarget;
	}

	public Vector2 GetTargetNFromPosition(Vector2 opposingPosition, float distance)
	{
		Vector2 newTarget = Host.Position;
		Vector2 direction = opposingPosition - Host.Position;
		Vector2 directionNormal = direction.Normalized();
		newTarget += (-directionNormal * distance);
		return newTarget;
	}

	// public void MoveToTarget()
	// {
	// 	bool up = false;
	// 	bool down = false;
	// 	bool left = false;
	// 	bool right = false;
	// 	// decide where the next move target is, and which direction to move to get there
	// 	if (Math.Abs(CurrentTarget.y - Host.Position.y) > DISTANCE_BUFFER)
	// 	{
	// 		up = CurrentTarget.y < Host.Position.y;
	// 		down = CurrentTarget.y > Host.Position.y;
	// 	}
	// 	if (Math.Abs(CurrentTarget.x - Host.Position.x) > DISTANCE_BUFFER)
	// 	{
	// 		left = CurrentTarget.x < Host.Position.x;
	// 		right = CurrentTarget.x > Host.Position.x;
	// 	}
	// 	Host.HostControlState.TryMoveDir = new Vector2( left ? -1 : (right ? 1 : 0), up ? -1 : (down ? 1 : 0));
	// }

	public void CalculateVelocity()
	{
		float distance = Host.Position.DistanceTo(CurrentTarget);

		Velocity += Steer();

		Vector2 postSteerVel = Velocity;

		Velocity += Avoid();
		// limit velocity to poststeerVel
		if (Velocity.Length() > postSteerVel.Length())
		{
			Velocity /= (Velocity.Length() / postSteerVel.Length());
		}

	}
	

	// https://howtorts.github.io/2014/01/14/avoidance-behaviours.html
	public Vector2 Avoid()
	{
		
		// If we are moving slowly, don't need to avoid
		// if (Velocity.Length() < 10)
		// {

		// 	// GD.Print(Velocity.Length());
		// 	return new Vector2(0,0);
		// }

		// Make a totalavoidanceforce variable to add all the avoidance forces from the rays
		Vector2 totalAvoidanceForce = new Vector2(0,0);

		// Get all the raycasts
		foreach (RayCast2D ray in Host.CollisionRays)
		{
			
			// If the raycast isn't colliding, do nothing for this raycast
			if (! ray.IsColliding())
				continue;

			// if the raycast is colliding with parasite, do nothing
			if (ray.IsColliding())
			{
				if (ray.GetCollider() is Parasite p)
				{
					return new Vector2(0,0);
				}
			}

			// if (ray.Collider)

			Node2D collider = (Node2D) ray.GetCollider();
			// GD.Print(collider);

			// Vector2 colliderVel = new Vector2(0,0);

			// // Add our velocity to the collider's velocity
			// // If this makes the total length longer than the individual length of one of them,
			// // then we are going in the same direction
			// // Vector2 colliderVel = collider.ShipState.CurrentVelocity;
			// Vector2 combinedVel = colliderVel + Velocity;
			// float combinedVelSquared = combinedVel.LengthSquared();

			// // If we are going in the same direction
			// if (combinedVelSquared > Velocity.LengthSquared ())
			// {
			// 	// We don't need to avoid for this raycast
			// 	continue;
			// }

			// We need to steer to go around the collider
			Vector2 vectorInOtherDirection = Host.GlobalPosition - collider.GlobalPosition;

			// Are we more left or right of them?
			bool isLeft = true;
			// https://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another
			float dot = Velocity.x * -vectorInOtherDirection.y + Velocity.y * vectorInOtherDirection.x;
			isLeft = dot > 0;
			
			// Calculate a right angle of the vector between us
			Vector2 resultVector = isLeft ? new Vector2(-vectorInOtherDirection.y, vectorInOtherDirection.x) 
				: new Vector2(vectorInOtherDirection.y, -vectorInOtherDirection.x);
			// resultVector = resultVector.Normalized(); // we already do this below

			totalAvoidanceForce += ScaleAvoidanceForce(resultVector, collider.Position - Host.Position);

		}
		// GD.Print(totalAvoidanceForce);
		return totalAvoidanceForce;
	}

	public Vector2 ScaleAvoidanceForce(Vector2 avoidanceForce, Vector2 shipToCollider)
	{
		// Normalise then scale by scalar and max avoid force below
		avoidanceForce = avoidanceForce.Normalized();

		float distance = shipToCollider.Length();

		// Scale a value depending on the inverse of distance between host and collider, keping between 5 and 60
		float scalar = Math.Min(60, Math.Max(5, 90*SteerForce)) / distance;
		// Multiply this by a maximum force
		float maxAvoidForce = (float)Math.Sqrt(Host.Speed)*300;
		scalar *= maxAvoidForce;
		avoidanceForce *= scalar;

		return avoidanceForce;
	}

	// provide a steering force
	public Vector2 Steer()//Vector2 desired, Vector2 velocity)
	{		
		// Vector2 originalVel = CurrentVelocity;
		
		// calculate the desired velocity
		Vector2 desiredVel = (CurrentTarget - Host.Position).Normalized() * Host.Speed;


		// incorporate Arrive behaviour
		// if our target is close and we aren't charging, then we want to stop so decelerate
		// float distance = Host.Position.DistanceTo(CurrentTarget);
		// if (distance < 500)
		// {
		// 	float decelMultiplier = Math.Min( (1/(Deceleration))*distance,1);
		// 	desiredVel *= decelMultiplier;
		// }
		
		// calculating the steering force to take us there
		Vector2 steering = desiredVel - Velocity;

		// adjust our velocity by adding the steering force
		return (steering * SteerForce);
	}

	public void OrientAnimation()
	{
		Host.HostActionState.SetAnim(Host.Position + Velocity);
		Host.HostActionState.RotateForward(Host.Position + Velocity);
		// Host.HostActionState.SetAnim(CurrentTarget);
		// Host.HostActionState.RotateForward(CurrentTarget);
		// Host.Rotation = Host.Position.AngleToPoint(Host.Position + Velocity) + (float)Math.PI;// - 1.5708f;
		// if (Velocity.Length() > 250f)
			// Host.LookAt(CurrentTarget);
			// Host.GetNode<Label>("LblAIState").RectRotation = -Host.RotationDegrees;
		// foreach (Node n in Host.GetChildren())
		// {
		// 	if (n is Node2D node2D)
		// 	{
		// 		node2D.LookAt(CurrentTarget);
		// 	}
		// }
		// foreach (RayCast2D ray in Host.DetectRays)
		// {
		// 	ray.LookAt(CurrentTarget);
		// }
	}

	public void RotateTowardsTarget()
	{
		foreach (Node n in Host.GetChildren())
		{
			if (n is Node2D node2D)
			{
				node2D.LookAt(CurrentTarget);
			}
		}
	}



	public void MoveCollide(float delta)
	{
		KinematicCollision2D collisionData = Host.MoveAndCollide(Velocity * delta);
	}
}
