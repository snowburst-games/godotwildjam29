using Godot;
using System;
using System.Linq;
public class HostActionState
{

	public Host Host {get; set;}

	public KinematicCollision2D CollisionData {get; set;}

	public virtual void Update(float delta)
	{
		if (Host.GetActionState() != Host.ActionState.Dying)
		{
			if (Host.Health <= 0)
			{
				Host.SetActionState(Host.ActionState.Dying);
			}
		}
	}

	public virtual void Exit()
	{
		
	}


	public void SetAnim(Vector2 nextPos)
	{
		Vector2 direction = nextPos - Host.Position;
		if (direction.x > 0)
		{
			Host.FlipH(true);
		}
		else if (direction.x < 0)
		{
			Host.FlipH(false);
		}
		if (Mathf.Abs(direction.y) + 0.1f > Mathf.Abs(direction.x))
		{
			if (direction.y < 0)
			{
				Host.PlayAnimFrame("WalkUp");
			}
			else if (direction.y > 0)
			{
				Host.PlayAnimFrame("WalkDown");
			}
		}
		else if (Mathf.Abs(direction.x) > 0)
		{
			Host.PlayAnimFrame("WalkSide");
			
		}
		if (direction == new Vector2(0,0))
		{
			Host.PlayAnimFrame("Idle");
			Host.StopSoundEffectMovement();
		}
		else
		{
			Host.PlaySoundMovement(AudioData.HostSound.Footsteps);
		}
	}

	public void RotateForward(Vector2 nextPos)
	{
		if (nextPos == Host.Position)
		{
			return;
		}

		Vector2 direction = nextPos - Host.Position;
		// if (Host.HostControlState.TryMoveDir != new Vector2(0,0))
		// {
			foreach (Node n in Host.GetChildren())
			{
				if (n is Node2D node2D)
				{
					// if (node2D.GetChildCount() == 1)
					// {
					// 	if (node2D.GetChild(0).Name == "SuspectArea")
					// 	{
					// 		((Node2D)node2D.GetChild(0)).LookAt(nextPos);
					// 	}
					if (node2D.Name == "Sprite" || node2D.Name == "Shape" || node2D.Name == "HostDetectArea" || node2D is CPUParticles2D)//"LevelUpParticles")
					{
						continue; // dont rotate the sprite
					}
					// if (node2D.Name == "Shape")
					// {
					// 	continue;
					// }
					// }
					node2D.LookAt(nextPos);
					if (Host.DetectRays.Contains(node2D) || node2D.Name == "CntSuspectArea")
					{
						if (direction.x > 0 && Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
						{
							node2D.Position = new Vector2(15,0);
						}
						if (direction.x < 0 && Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
						{
							node2D.Position = new Vector2(-15,0);
						}
						if (direction.y > 0 && Mathf.Abs(direction.y) > Mathf.Abs(direction.x))
						{
							node2D.Position = new Vector2(0, 30);
						}
						if (direction.y < 0 && Mathf.Abs(direction.y) > Mathf.Abs(direction.x))
						{
							node2D.Position = new Vector2(0, -30);
						}
						// if (node2D.RotationDegrees > -65)
						// {
						// 	node2D.Position = new Vector2(15, 0);
						// }
						// if (node2D.RotationDegrees > 65)
						// {
						// 	node2D.Position = new Vector2(0, 30);
						// }
						// if (node2D.RotationDegrees > 150)
						// {
						// 	node2D.Position = new Vector2(-15, 0);
						// }
						// if (node2D.RotationDegrees > 260)
						// {
						// 	node2D.Position = new Vector2(0, -30);
						// }
						// if (node2D.RotationDegrees > 330 && node2D.RotationDegrees < 360)
						// {
						// 	node2D.Position = new Vector2(15,0);
						// }
					}
				}
			}
		// }
	}
}
