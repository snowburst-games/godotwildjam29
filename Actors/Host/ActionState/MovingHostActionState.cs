using Godot;
using System;

public class MovingHostActionState : HostActionState
{
	public MovingHostActionState()
	{

	}
	public MovingHostActionState(Host host)
	{
		this.Host = host;
	}

	public override void Update(float delta)
	{
		base.Update(delta);

		CollisionData = Host.MoveAndCollide(Host.HostControlState.TryMoveDir.Normalized() * Host.Speed * delta);
		RotateForward(Host.Position + Host.HostControlState.TryMoveDir.Normalized() * Host.Speed);

		if (Host.HostControlState.TryMoveDir == new Vector2(0,0))
		{
			Host.SetActionState(Host.ActionState.Idle);
		}

		SetAnim(Host.Position + Host.HostControlState.TryMoveDir.Normalized() * Host.Speed);
	}

}
// Sprite/SuspectArea
