using Godot;
using System;

public class IdleHostActionState : HostActionState
{
	public IdleHostActionState()
	{

	}
	public IdleHostActionState(Host host)
	{
		this.Host = host;
	}

	public override void Update(float delta)
	{
		base.Update(delta);
		if (Host.HostControlState.TryMoveDir != new Vector2(0,0))
		{
			Host.SetActionState(Host.ActionState.Moving);
		}
	}
}
