using Godot;
using System;

public class DyingHostActionState : HostActionState
{
	public DyingHostActionState()
	{

	}
	public DyingHostActionState(Host host)
	{
		this.Host = host;
	}

	public override void Update(float delta)
	{
		base.Update(delta);

		// GD.Print("I AM DYING");
		Host.Die();
	}
}
