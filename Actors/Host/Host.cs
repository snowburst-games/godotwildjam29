// Need player state and AI state (state switches to player when infested)
// player state - normal movement. if not (exported) PerfectHost then die after a minute

using Godot;
using System;
using System.Collections.Generic;

public class Host : KinematicBody2D
{	
	public Dictionary<AudioData.HostSound, AudioStream> HostSoundPaths {get; set;} = AudioData.LoadedSounds<AudioData.HostSound>(AudioData.HostSoundPaths);

	public const float STARTING_SPEED = 300f;
	public float Speed {get; set;} = 300f;
	public const float HEALTH_DRAIN = 10f;
	public float Health {get; set;} = 100f;
	public float StealthModifier {get; set;} = 1f;

	[Export]
	public bool IsPerfectHost {get; set;} = false;
	[Export]
	public bool IsContaminated {get; set;} = false;
	[Export]
	private _clothesColour _currentClothesColour = _clothesColour.Purple;

	public Vector2 StartPosition;

	public enum ControlState { AIState, PlayerState }
	public HostControlState HostControlState {get; set;}
	public enum ActionState { Idle, Moving, Dying }
	public HostActionState HostActionState {get; set;}
	public bool IsSuspicious {get; set;} = false;

	public Timer IdleAITimer {get; set;}
	private Camera2D Cam {get; set;}
	private AnimationPlayer _anim;
	private AnimationPlayer _animFrames;
	private Sprite _sprite;
	public Timer InfestTimer {get; set;}
	public Sprite SpriteSuspectArea {get; set;}
	public ProgressBar SuspicionBar {get; set;}
	public RayCast2D[] CollisionRays {get; set;}
	public RayCast2D[] DetectRays {get; set;}
	private AudioStreamPlayer2D _soundPlayer;
	private AudioStreamPlayer2D _soundPlayerMovement;
	private AudioStreamPlayer2D _voicePlayer;
	

	private enum _clothesColour {
		Purple, Green, Pink, Grey, White, Blue
	}

	private Dictionary<_clothesColour, Texture> _spriteTextures;

	[Signal]
	public delegate void ParasiteCaught();
	[Signal]
	public delegate void Died(Vector2 position, Host host);
	[Signal]
	public delegate void LeftHost(Vector2 position);
	[Signal]
	public delegate void PerfectHostFound();
	[Signal]
	public delegate void ContaminatedHostEntered();
	[Signal]
	public delegate void BecameSuspicious(Vector2 Position);

	public override void _Ready()
	{
		ReferenceNodes();
		StartPosition = Position;
		SetControlState(ControlState.AIState);
		SetActionState(ActionState.Idle);
		SuspicionBar.Visible = false;

		if (IsPerfectHost)
		{
			IsContaminated = false;
		}

		// Testing player:
		// SetControlState(ControlState.PlayerState);
	}

	private void ReferenceNodes()
	{
		_spriteTextures = new Dictionary<_clothesColour, Texture>() {
			{_clothesColour.Purple, ((Texture)GD.Load("res://Actors/Host/Art/HOST_1 150121.png"))},
			{_clothesColour.Green, ((Texture)GD.Load("res://Actors/Host/Art/HOST_2 150121.png"))},
			{_clothesColour.Pink, ((Texture)GD.Load("res://Actors/Host/Art/HOST_3 150121.png"))},
			{_clothesColour.Grey, ((Texture)GD.Load("res://Actors/Host/Art/HOST_4 150121.png"))},
			{_clothesColour.White, ((Texture)GD.Load("res://Actors/Host/Art/HOST_5 150121.png"))},
			{_clothesColour.Blue, ((Texture)GD.Load("res://Actors/Host/Art/HOST_6 150121.png"))}
		};

		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		_voicePlayer = GetNode<AudioStreamPlayer2D>("VoicePlayer");
		_soundPlayerMovement = GetNode<AudioStreamPlayer2D>("SoundPlayerMovement");
		IdleAITimer = GetNode<Timer>("IdleAITimer");
		InfestTimer = GetNode<Timer>("InfestTimer");
		Cam = GetNode<Camera2D>("Camera");
		_anim = GetNode<AnimationPlayer>("Anim");
		_animFrames = GetNode<AnimationPlayer>("AnimFrames");
		_sprite = GetNode<Sprite>("Sprite");
		_sprite.Texture = _spriteTextures[_currentClothesColour];
		SuspicionBar = GetNode<ProgressBar>("SuspicionBar");
		SpriteSuspectArea = GetNode<Sprite>("CntSuspectArea/SuspectArea");
		CollisionRays = new RayCast2D[8] { GetNode<RayCast2D>("CollisionRayDown"), GetNode<RayCast2D>("CollisionRayUp"), GetNode<RayCast2D>("CollisionRayLeft"), GetNode<RayCast2D>("CollisionRayRight"),
		GetNode<RayCast2D>("CollisionRayDownLeft"),GetNode<RayCast2D>("CollisionRayDownRight"),GetNode<RayCast2D>("CollisionRayUpLeft"),GetNode<RayCast2D>("CollisionRayUpRight")};
		DetectRays = new RayCast2D[15] { GetNode<RayCast2D>("DetectRay"), GetNode<RayCast2D>("DetectRay2"), GetNode<RayCast2D>("DetectRay3"), GetNode<RayCast2D>("DetectRay4"),
		GetNode<RayCast2D>("DetectRay5"),GetNode<RayCast2D>("DetectRay6"),GetNode<RayCast2D>("DetectRay7"),GetNode<RayCast2D>("DetectRay8"),GetNode<RayCast2D>("DetectRay9"),
		GetNode<RayCast2D>("DetectRay10"),GetNode<RayCast2D>("DetectRay11"),GetNode<RayCast2D>("DetectRay12"),GetNode<RayCast2D>("DetectRay13"),GetNode<RayCast2D>("DetectRay14"),
		GetNode<RayCast2D>("DetectRay15") };
	}

	public void SetControlState(ControlState state)
	{
		if (HostControlState != null)
		{
			HostControlState.Exit();
		}
		switch (state)
		{
			case ControlState.AIState:
				HostControlState = new AIHostControlState(this);
				break;
			case ControlState.PlayerState:
				HostControlState = new PlayerHostControlState(this);
				break;
		}
	}

	public void SetActionState(ActionState state)
	{
		if (HostActionState != null)
		{
			HostActionState.Exit();
		}
		switch (state)
		{
			case ActionState.Idle:
				HostActionState = new IdleHostActionState(this);
				break;
			case ActionState.Moving:
				HostActionState = new MovingHostActionState(this);
				break;
			case ActionState.Dying:
				HostActionState = new DyingHostActionState(this);
				break;
			// case ActionState.Suspecting:
			// 	HostActionState = new SuspectingHostActionState(this);
			// 	break;
		}
	}

	public ControlState GetControlState()
	{
		return HostControlState is AIHostControlState ? ControlState.AIState : ControlState.PlayerState;
	}

	public ActionState GetActionState()
	{
		if (HostActionState is IdleHostActionState)
		{
			return ActionState.Idle;
		}
		else if (HostActionState is MovingHostActionState)
		{
			return ActionState.Moving;
		}
		else// (HostActionState is DyingHostActionState)
		{
			return ActionState.Dying;
		}
		// else
		// {
		// 	return ActionState.Suspecting;
		// }
	}

	private bool _died = false;

	public async void Die()
	{
		if (_died)
		{
			return;
		}
		_died = true;
		PlayAnim("Die");
		PlaySoundEffect(AudioData.HostSound.Explode);
		await ToSignal(_anim, "animation_finished");
		Cam.Current = false;
		EmitSignal(nameof(Died), GlobalPosition, this);
		QueueFree();
	}

	public void LeaveHost()
	{
		Cam.Current = false;
		EmitSignal(nameof(LeftHost), GlobalPosition);
		SetControlState(ControlState.AIState);
		SetActionState(ActionState.Idle);
		Flee(GlobalPosition);
	}

	public void Flee(Vector2 position)
	{
		// float distance = Position.DistanceTo(position);
		if (HostControlState is AIHostControlState aiHost)
		{
			FleeFromHere = position;
			aiHost.SetAIState(AIHostControlState.AIState.Fleeing);
		}
	}

	public void OnNearbyHostSuspicious(Vector2 parasitePosition)
	{
		if (HostControlState is AIHostControlState aiHost)
		{
			NearbyHostSuspicious = true;
			// ParasitePositionFromNearbyHost = parasitePosition;
			aiHost.SetAIState(AIHostControlState.AIState.Suspecting);
			aiHost.HostAIState.LastKnownTarget = parasitePosition;
		}
	}

	public Vector2 FleeFromHere {get; set;}
	// public Vector2 ParasitePositionFromNearbyHost {get; set;}
	public bool NearbyHostSuspicious {get; set;} = false;

	public async void Infest()
	{
		PlayAnim("Infest");
		await ToSignal(_anim, "animation_finished");
		SetControlState(ControlState.PlayerState);
		Cam.Current = true;
	}

	public void OnStealthPowerUp()
	{
		StealthModifier = Mathf.Clamp(0.3f, StealthModifier - 0.1f, 1f);
	}

	public override void _PhysicsProcess(float delta)
	{
		if (HostControlState == null || HostActionState == null)
		{
			return;
		}
		HostControlState.Update(delta);
		HostActionState.Update(delta);
	}

	public void PlayAnim(string animName)
	{
		if (_anim.IsPlaying() && _anim.CurrentAnimation == animName)
		{
			return;
		}
		_anim.Play(animName);
	}

	public void FlipH(bool flipH)
	{
		_sprite.FlipH = flipH;
	}


	public void PlaySoundEffect(AudioData.HostSound hostSound, AudioData.SoundBus bus = AudioData.SoundBus.Effects)
	{
		if (_soundPlayer.Playing && _soundPlayer.Stream == HostSoundPaths[hostSound])
		{
			return;
		}
		AudioHandler.PlaySound(_soundPlayer, HostSoundPaths[hostSound], bus);
	}

	public void PlayVoice(AudioData.HostSound hostSound)
	{
		if (_voicePlayer.Playing && _voicePlayer.Stream == HostSoundPaths[hostSound])
		{
			return;
		}
		AudioHandler.PlaySound(_soundPlayer, HostSoundPaths[hostSound], AudioData.SoundBus.Voice);
	}
	public void PlaySoundMovement(AudioData.HostSound hostSound)
	{
		if (_soundPlayerMovement.Playing && _soundPlayerMovement.Stream == HostSoundPaths[hostSound])
		{
			return;
		}
		// _soundPlayerMovement.VolumeDb = -15;
		AudioHandler.PlaySound(_soundPlayerMovement, HostSoundPaths[hostSound], AudioData.SoundBus.Effects);
	}

	public void StopSoundEffectMovement()
	{
		// _soundPlayerMovement.Stream = null;
		// _soundPlayerMovement.VolumeDb = -1000;
		// _soundPlayerMovement.Stream = ((AudioStream)GD.Load("res://Actors/Common/empty.wav"));
		_soundPlayerMovement.Stop();
		// GD.Print(_soundPlayerMovement.Stream.GetLength());
	}

	public void PlayAnimFrame(string animName)
	{
		if (_animFrames.IsPlaying() && _animFrames.CurrentAnimation == animName)
		{
			return;
		}
		_animFrames.Play(animName);
	}

	public async void OnContaminatedHostEntered()
	{
		PlayAnim("Die");
		await ToSignal(_anim, "animation_finished");
		EmitSignal(nameof(ContaminatedHostEntered));
	}

	private void OnAnimAnimationFinished(String anim_name)
	{
		if (anim_name == "PerfectHost")
		{
			EmitSignal(nameof(PerfectHostFound));
		}
		if (anim_name == "Caught")
		{
			EmitSignal(nameof(ParasiteCaught));
		}
		// if (anim_name == "ContaminatedHost")
		// {
		// 	EmitSignal(nameof(ContaminatedHostEntered));
		// }
	}


}
