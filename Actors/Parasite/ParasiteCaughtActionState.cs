using Godot;
using System;

public class ParasiteCaughtActionState : ParasiteActionState
{

	public ParasiteCaughtActionState(Parasite parasite)
	{
		this.Parasite = parasite;
		// GD.Print("Caught + play death animation");
	}

	public override void Update(float delta)
	{
		base.Update(delta);
	}

	public override void _Process(float delta)
	{
		
	}
}
