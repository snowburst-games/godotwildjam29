using Godot;
using System;

public class ParasiteIdleActionState : ParasiteActionState
{
	

	public ParasiteIdleActionState(Parasite Parasite)
	{
		this.Parasite = Parasite;
		// GD.Print("idle action state");
		Parasite.PlayAnim("Idle");
	}

	public override void Update(float delta)
	{
		base.Update(delta);
		if (Parasite.MoveDir.x > 0 || Parasite.MoveDir.x < 0 || Parasite.MoveDir.y > 0 || Parasite.MoveDir.y < 0)
		{
			// GD.Print("Idle change state to moving");
			Parasite.ChangeState(Parasite.State.Moving);
		}
		if (Parasite.MoveDir.x == 0)
		{
			// GD.Print("Not moving");
		}	
		if (!Parasite.Visible)
		{
			Parasite.Activate();
		}
	}
	
	public override void _Process(float delta)
	{
	}

 
}
