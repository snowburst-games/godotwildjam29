using Godot;
using System;
using System.Collections.Generic;

public class ParasiteMovingActionState : ParasiteActionState
{
	public Vector2 DiagonalDownLeft = new Vector2 (-1,1);
	public Vector2 DiagonalUpLeft = new Vector2 (-1,-1);
	public Vector2 DiagonalDownRight = new Vector2 (1,1);
	public Vector2 DiagonalUpRight = new Vector2 (1,-1);
	public Vector2 Down = new Vector2 (0,1);
	public Vector2 Up = new Vector2 (0,-1);
	public Vector2 Right = new Vector2 (1,0);
	public Vector2 Left = new Vector2 (-1,0);
//	List<RayCast2D> list = new List<RayCast2D>();
//	public int RotationDegrees;

	public ParasiteMovingActionState(Parasite parasite)
	{
		this.Parasite = parasite;
		// GD.Print("Moving");
		Parasite.Anim.Play("Right");
	}

	private bool _stopped = false;

	public override void Update(float delta)
	{
		base.Update(delta);
		Parasite.MoveAndCollide(Parasite.MoveDir.Normalized() * Parasite.Speed * delta);
		if (Parasite.MoveDir == new Vector2 (0,0)) //if the parasite stops moving, change it back to IDLE
		{
			Parasite.ChangeState(Parasite.State.Idle);
		}
		if (!_stopped)
		{
			Parasite.PlaySoundMovement(AudioData.WormSound.Movement);
		}
	/* 	if ((Parasite.MoveDir.y>0) && ((Parasite.MoveDir.x<0) || (Parasite.MoveDir.x>0)))
		{
			Parasite.FlipLeft(false);
			Parasite.PlayAnim("DownDiag");
			Parasite.RayCast2D.RotationDegrees = 90;	
			GD.Print("diagnal movemnt");
		} */
		if ((Parasite.MoveDir == DiagonalDownRight))
		{
			Parasite.FlipLeft(false);
			Parasite.PlayAnim("Down");
			Parasite.RotationDegreez = 90 + 45;	
		}
		if ((Parasite.MoveDir == DiagonalUpRight))
		{
			Parasite.FlipLeft(false);
			Parasite.PlayAnim("Up");
			Parasite.RotationDegreez = 45;	
		}
		if ((Parasite.MoveDir == DiagonalDownLeft))
		{
			Parasite.FlipLeft(false);
			Parasite.PlayAnim("Down");
			Parasite.RotationDegreez = 180+45;	
		}
		if ((Parasite.MoveDir == DiagonalUpLeft))
		{
			Parasite.FlipLeft(false);
			Parasite.PlayAnim("Up");
			Parasite.RotationDegreez = 270+45;	
		}
		if (Parasite.MoveDir == Right)
		{
			Parasite.FlipLeft(false);
			Parasite.PlayAnim("Right");
			Parasite.RotationDegreez = 90; //rotate the raycast as the worm rotates
		}
		if (Parasite.MoveDir==Left)
		{
			Parasite.FlipLeft(true);
			Parasite.PlayAnim("Right");
			Parasite.RotationDegreez = 270;
		}
		if (Parasite.MoveDir == Up)
		{
			Parasite.FlipLeft(false);
			Parasite.PlayAnim("Up");
			Parasite.RotationDegreez = 360;
		}
		if (Parasite.MoveDir == Down)
		{
			Parasite.FlipLeft(false);
			Parasite.PlayAnim("Down");
			Parasite.RotationDegreez = 180;	
		}
	//	Parasite.RotateRays(); //Disabled Rotation of Rays because I 
	//want to test if infestation feels better with Raycasts all around

	}

	public override void Exit()
	{
		// GD.Print("stop the sound!");
		_stopped = true;
		Parasite.StopSoundEffectMovement();
	}




  public override void _Process(float delta)
  {
	 	
  }
}
