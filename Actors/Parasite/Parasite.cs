using Godot;
using System;
using System.Collections.Generic;

public class Parasite : KinematicBody2D
{	
	public Dictionary<AudioData.WormSound, AudioStream> WormSoundPaths {get; set;} = AudioData.LoadedSounds<AudioData.WormSound>(AudioData.WormSoundPaths);
	public ParasiteActionState ParasiteActionState {get; set;}
	public Vector2 MoveDir;
	public float Speed {get; set;} = 300;
	public float MaxSpeed {get; set;} = 800;
	public float MinSpeed {get; set;} = 300;
	public RayCast2D Ray1;
	public RayCast2D Ray2;
	public RayCast2D Ray3;
	public RayCast2D Ray4;
	public RayCast2D Ray5;
	public RayCast2D Ray6;
	public RayCast2D Ray7;
	public RayCast2D Ray8;
	public Host Host;
	public Camera2D Camera2D;
	public CollisionShape2D CollisionShape2D;
	public AnimationPlayer Anim;
	private AnimationPlayer _animEffects;
	public Sprite Sprite;
	public CollisionShape2D DetectArea;
	List<RayCast2D> list = new List<RayCast2D>();
	public int RotationDegreez = 0;
	// private bool _infesting = false;
	private AudioStreamPlayer2D _soundPlayer;
	private AudioStreamPlayer2D _soundPlayerMovement;


	public override void _Ready()
	{
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		_soundPlayerMovement = GetNode<AudioStreamPlayer2D>("SoundPlayerMovement");
		//var hosts = GetTree().GetNodesInGroup("CntHosts");
		Ray1= (RayCast2D)GetNode("RayCast2D");
		Ray2= (RayCast2D)GetNode("RayCast2D2");
		Ray3= (RayCast2D)GetNode("RayCast2D3");
		Ray4= (RayCast2D)GetNode("RayCast2D4");
		Ray5= (RayCast2D)GetNode("RayCast2D5");
		Ray6= (RayCast2D)GetNode("RayCast2D6");
		Ray7= (RayCast2D)GetNode("RayCast2D7");
		Ray8= (RayCast2D)GetNode("RayCast2D8");
		Camera2D = (Camera2D)GetNode("Cam");
		CollisionShape2D = (CollisionShape2D)GetNode("CollisionShape2D");
		Anim = (AnimationPlayer)GetNode("Anim");
		_animEffects = GetNode<AnimationPlayer>("AnimEffects");
		Sprite = (Sprite)GetNode("Sprite");
		DetectArea = (CollisionShape2D)GetNode("DetectArea/CollisionShape2D");
		MakeRayCast2DList();
		ChangeState(State.Idle); //Start as IDLE
	}
	public void MakeRayCast2DList()
	{
		list.Add(Ray1);
		list.Add(Ray2);
		list.Add(Ray3);
		list.Add(Ray4);
		list.Add(Ray5);
		list.Add(Ray6);
		list.Add(Ray7);
		list.Add(Ray8);
	}

	public void RotateRays()
	{
		foreach (RayCast2D ray in list)
		{
			ray.RotationDegrees = RotationDegreez;
		}
	}

	public void FlipLeft(bool flip)
	{
		Sprite.FlipH = flip;	
	}

	public void PlayAnim(string anim)
	{
		if (Anim.CurrentAnimation == anim)
		{
			return;
		}
		Anim.Play(anim);
	}

	public void PlayAnimEffects(string anim)
	{
		if (_animEffects.IsPlaying() && _animEffects.CurrentAnimation == anim)
		{
			return;
		}
		_animEffects.Play(anim);
	}

	public void OnPowerupActivated()
	{
		PlayAnimEffects("Powerup");
		PlaySoundEffect(AudioData.WormSound.PowerupPickup);
	}

	public void OnHintNoteActivated()
	{
		PlayAnimEffects("Powerup");
		PlaySoundEffect(AudioData.WormSound.HintPickup);
	}

	public void OnSpeedPowerUp()
	{
		float speedBonus = 40;
		Speed = Mathf.Clamp(Speed + speedBonus, MinSpeed, MaxSpeed);
	}

	public bool IsColliderAHost() //Find out if the collider is a kinematic 2D (alternatively can get host.Name and see if it is Host-Host6)
	{
		foreach (RayCast2D ray in list)
		{
			if (ray.GetCollider() is Area2D hostDetectArea)
			{
				if (hostDetectArea.Name == "HostDetectArea")
				{
					return true;
				}
			}
		}
		return false;
	}

	public void PlaySoundEffect(AudioData.WormSound wormSound, AudioData.SoundBus bus = AudioData.SoundBus.Effects)
	{
		if (_soundPlayer.Playing && _soundPlayer.Stream == WormSoundPaths[wormSound])
		{
			return;
		}
		AudioHandler.PlaySound(_soundPlayer, WormSoundPaths[wormSound], bus);
	}
	public void PlaySoundMovement(AudioData.WormSound wormSound)
	{
		if (_soundPlayerMovement.Playing && _soundPlayerMovement.Stream == WormSoundPaths[wormSound])
		{
			return;
		}
		// _soundPlayerMovement.VolumeDb = -15;
		AudioHandler.PlaySound(_soundPlayerMovement, WormSoundPaths[wormSound], AudioData.SoundBus.Effects);
	}

	public void StopSoundEffectMovement()
	{
		// _soundPlayerMovement.Stream = null;
		// _soundPlayerMovement.VolumeDb = -1000;
		// _soundPlayerMovement.Stream = ((AudioStream)GD.Load("res://Actors/Common/empty.wav"));
		_soundPlayerMovement.Stop();
		// GD.Print(_soundPlayerMovement.Stream.GetLength());
	}

	public bool IsHostSuspicious()
	{
		 foreach (RayCast2D ray in list)
		 {
			 if (ray.IsColliding())
			 {
				if (ray.GetCollider() is Area2D hostDetectArea)
				{
					if (hostDetectArea.Name == "HostDetectArea")
					{
						if (((Host)hostDetectArea.GetParent()).IsSuspicious == true)
						{
							return true;
						}
					}
				 }
			 }
		 }
		 return false;
	}
	
	public async void InfestHost()
	{
		foreach (RayCast2D ray in list)
		{
			if (ray.IsColliding())
			{
				if (ray.GetCollider() is Area2D hostDetectArea)
				{
					if (hostDetectArea.Name == "HostDetectArea")
					{
						
						// _infesting = true;
						// if (hostDetectArea == null)
						// {
						// 	return;
						// }
						// if (hostDetectArea.GetParent() == null)
						// {
						// 	return;
						// }
						Host h = ((Host)hostDetectArea.GetParent());
						if (h.IsPerfectHost)
						{
							PlaySoundEffect(AudioData.WormSound.InfestPerfectHost);
						}
						else if (h.IsContaminated)
						{
							PlaySoundEffect(AudioData.WormSound.InfestContaminatedHost);
						}
						else
						{
							PlaySoundEffect(AudioData.WormSound.InfestSuccess);
						}
						h.Infest();
						Camera2D.Current=false;
						await ToSignal(_animEffects, "animation_finished");
						Deactivate(); 
					}
				}
			}
		}
	}

	public enum State //enum defines a list of variables
	{
		Idle ,
		Moving,
		Infesting,
		Caught
	};

	public void ChangeState(State state) //Access these from the state machine
	{
		if (ParasiteActionState != null)
		{
			ParasiteActionState.Exit();
		}
		switch(state) 
		{
			case State.Idle:
				ParasiteActionState = new ParasiteIdleActionState(this); //change these after you make script
				break;
 			case State.Moving:
				ParasiteActionState = new ParasiteMovingActionState(this);
				break;
			case State.Infesting:
				ParasiteActionState = new ParasiteInfestingActionState(this);
				break;
			case State.Caught:
				ParasiteActionState = new ParasiteCaughtActionState(this);
				break; 
		}
	}

	public State GetState() //access this from the state machine
	{
		if (ParasiteActionState is ParasiteIdleActionState)
		{
			return State.Idle;
		}
		else if (ParasiteActionState is ParasiteMovingActionState)
		{
			return State.Moving;
		}
		else if (ParasiteActionState is ParasiteInfestingActionState)
		{
			return State.Infesting;
		}
		else
		{
			return State.Caught;
		}
	}

	public void OnCaught() //If caught, change to caught state (host will communicate this)
	{
		ChangeState(State.Caught);
	}

	public async void OnHostDied(Vector2 position) //when the host dies, set new parasite postion and show parasite
	{
		// _infesting = false;
		GetNode<Timer>("InfestTimer").Start();
		GD.Print(position + " died here");
		this.Position = position;
		PlayAnimEffects("LeftHost");
		PlaySoundEffect(AudioData.WormSound.LeaveHost);
		Activate();
		await ToSignal(_animEffects, "animation_finished");
		Camera2D.Current = true;
		ChangeState(State.Idle); //new
	}

	public override void _PhysicsProcess(float delta)
	{
		GetInput(); //detect user input (except E)
		ParasiteActionState.Update(delta); //Keep checking the state machine
	}

	public void GetInput() //Still need to figure out how to rotate the sprite
	{
		MoveDir = new Vector2();
		if ((Input.IsActionPressed("ui_down") && (Input.IsActionPressed("ui_left"))))
		{
			MoveDir.y = 1;
			MoveDir.x = -1;
		}
		if (Input.IsActionPressed("ui_up"))
		{
			MoveDir.y = -1;
		}
		if (Input.IsActionPressed("ui_down"))
		{
			MoveDir.y = 1;
		}
		if (Input.IsActionPressed("ui_left"))
		{
			MoveDir.x = -1;
		}
		if (Input.IsActionPressed("ui_right"))
		{
			MoveDir.x = 1;
		}
		if (GetNode<Timer>("InfestTimer").TimeLeft > 0 || ((GetState() == Parasite.State.Infesting)))
		{
			return;
		}
		if (Input.IsActionJustPressed("Interact") && (IsColliderAHost())) //and the player is close enough to the host and 
		{
			// if (_infesting)
			// {
			// 	return;
			// }
			if ((!IsHostSuspicious()))
			{
				ChangeState(Parasite.State.Infesting);
				
				//change to the infesting state only if the player is clos enough.
			}
			else
			{
				PlaySoundEffect(AudioData.WormSound.InfestFailed);
			}
		}
	}

	public void Deactivate()
	{
		Visible = false;
		CollisionShape2D.Disabled = true;
		DetectArea.Disabled = true;
		
	}

	public void Activate()
	{
		Visible = true;
		CollisionShape2D.Disabled = false;
		DetectArea.Disabled = false;
	}
}
