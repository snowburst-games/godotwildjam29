using Godot;
using System;

public class ParasiteInfestingActionState : ParasiteActionState
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	// Called when the node enters the scene tree for the first time.
	public ParasiteInfestingActionState(Parasite parasite)
	{
		this.Parasite = parasite;
		// GD.Print("infesting");
		Parasite.PlayAnimEffects("Infest");
		Parasite.InfestHost();
		//Hide the Sprite. Movement is already disabled because there is no way to go back to moving state from here
		//Parasite script with show the parasite and change the parasite to the position of the human when the human dies
	}

	public override void Update(float delta)
	{
		base.Update(delta);
		
	}

	public override void _Process(float delta)
	{
		
	}
}
