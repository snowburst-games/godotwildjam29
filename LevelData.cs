using Godot;
using System;
using System.Collections.Generic;

public class LevelData : IJSONSaveable
{
	public List<int> LevelsCompleted {get; set;}
}
