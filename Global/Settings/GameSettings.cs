// GameSettings singleton script: for holding reference to global settings, loading and saving them.
using System;
using System.Collections.Generic;
using Godot;
using System.Linq;
using System.Text.RegularExpressions;

public sealed class GameSettings
{

	#region Instancing code / singleton pattern code
	public static GameSettings Instance {get {return Nested.instance;}}

	private class Nested
	{
		static Nested()
		{
		}

		internal static readonly GameSettings instance = new GameSettings();
	}
	#endregion
	
	public Dictionary<int, string> PlayerNames {get; set;}
	public Dictionary<string, float> SoundVolume {get; set;}
	public Dictionary<Controls, string> InputEvents {get; private set;} 
	public Dictionary<Controls, string> InputEvents2 {get; private set;} 

	private GameSettings()
	{
		// Initialised with these defaults in constructor. Note at menu we run the LoadFromJSON method which overwrites these dicts 
		// if a settings file exists
		PlayerNames = new Dictionary<int, string>()
		{
			{1, "PlayerOne"},
			{2, "PlayerTwo"}
		};

		SoundVolume = new Dictionary<string, float>()
		{
			{AudioData.SoundBusStrings[AudioData.SoundBus.Voice], 100},
			{AudioData.SoundBusStrings[AudioData.SoundBus.Effects], 100},
			{AudioData.SoundBusStrings[AudioData.SoundBus.Music], 100},
			{AudioData.SoundBusStrings[AudioData.SoundBus.Master], 100}
		};

		// The project will crash if these InputEvents do not exist in the keymap.
		// InputEvents = new Dictionary<Controls, string>() // The string here is the scancode e.g. P1Left default A
		// {
		//     {Controls.P1Left, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P1Left")[0]).Scancode)},
		//     {Controls.P1Right, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P1Right")[0]).Scancode)},
		//     {Controls.P1Up, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P1Up")[0]).Scancode)},
		//     {Controls.P1Down, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P1Down")[0]).Scancode)},
		//     {Controls.P1Attack, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P1Attack")[0]).Scancode)},
		//     {Controls.P2Up, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P2Up")[0]).Scancode)},
		//     {Controls.P2Down, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P2Down")[0]).Scancode)},
		//     {Controls.P2Attack, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P2Attack")[0]).Scancode)},
		//     {Controls.P2Left, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P2Left")[0]).Scancode)},
		//     {Controls.P2Right, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P2Right")[0]).Scancode)},
		//     {Controls.P1Pause, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P1Pause")[0]).Scancode)},
		//     {Controls.P2Pause, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P2Pause")[0]).Scancode)},
		//     {Controls.P1UShields, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P1UShields")[0]).Scancode)},
		//     {Controls.P1UWeapons, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P1UWeapons")[0]).Scancode)},
		//     {Controls.P1USpeed, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P1USpeed")[0]).Scancode)},
		//     {Controls.P2UShields, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P2UShields")[0]).Scancode)},
		//     {Controls.P2UWeapons, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P2UWeapons")[0]).Scancode)},
		//     {Controls.P2USpeed, OS.GetScancodeString(((InputEventKey) InputMap.GetActionList("P2USpeed")[0]).Scancode)},

		// };

		//
		// TEST
		InputEvents = new Dictionary<Controls, string>()
		{
			{Controls.P1Left, (((InputEvent) InputMap.GetActionList("P1Left")[0]).AsText())},
			{Controls.P1Right, (((InputEvent) InputMap.GetActionList("P1Right")[0]).AsText())},
			{Controls.P1Up, (((InputEvent) InputMap.GetActionList("P1Up")[0]).AsText())},
			{Controls.P1Down, (((InputEvent) InputMap.GetActionList("P1Down")[0]).AsText())},
			{Controls.P1Attack, (((InputEvent) InputMap.GetActionList("P1Attack")[0]).AsText())},
			{Controls.P2Up, (((InputEvent) InputMap.GetActionList("P2Up")[0]).AsText())},
			{Controls.P2Down, (((InputEvent) InputMap.GetActionList("P2Down")[0]).AsText())},
			{Controls.P2Attack, (((InputEvent) InputMap.GetActionList("P2Attack")[0]).AsText())},
			{Controls.P2Left, (((InputEvent) InputMap.GetActionList("P2Left")[0]).AsText())},
			{Controls.P2Right, (((InputEvent) InputMap.GetActionList("P2Right")[0]).AsText())},
			{Controls.P1Pause, (((InputEvent) InputMap.GetActionList("P1Pause")[0]).AsText())},
			{Controls.P2Pause, (((InputEvent) InputMap.GetActionList("P2Pause")[0]).AsText())},
			{Controls.P1UShields, (((InputEvent) InputMap.GetActionList("P1UShields")[0]).AsText())},
			{Controls.P1UWeapons, (((InputEvent) InputMap.GetActionList("P1UWeapons")[0]).AsText())},
			{Controls.P1USpeed, (((InputEvent) InputMap.GetActionList("P1USpeed")[0]).AsText())},
			{Controls.P2UShields, (((InputEvent) InputMap.GetActionList("P2UShields")[0]).AsText())},
			{Controls.P2UWeapons, (((InputEvent) InputMap.GetActionList("P2UWeapons")[0]).AsText())},
			{Controls.P2USpeed, (((InputEvent) InputMap.GetActionList("P2USpeed")[0]).AsText())},
		};
	}

	public enum Controls{P1Left, P1Right, P1Up, P1Down, P1Attack, P2Left, P2Right, P2Up, P2Down, P2Attack, P1Pause, P2Pause, P1UShields, P1UWeapons, P1USpeed,
		P2UShields, P2UWeapons, P2USpeed}
	// This dict is just to allow us to get the string
	public Dictionary<Controls, string> InputActions {get;} = new Dictionary<Controls, string>()
	{   // The string here is the action name. Hence this dict and the above are distinct
		{Controls.P1Left, "P1Left"},
		{Controls.P1Right, "P1Right"},
		{Controls.P1Up, "P1Up"},
		{Controls.P1Down, "P1Down"},
		{Controls.P1Attack, "P1Attack"},
		{Controls.P2Left, "P2Left"},
		{Controls.P2Right, "P2Right"},
		{Controls.P2Up, "P2Up"},
		{Controls.P2Down, "P2Down"},
		{Controls.P2Attack, "P2Attack"},
		{Controls.P1Pause, "P1Pause"},
		{Controls.P2Pause, "P2Pause"},
		{Controls.P1UShields, "P1UShields"},
		{Controls.P1UWeapons, "P1UWeapons"},
		{Controls.P1USpeed, "P1USpeed"},
		{Controls.P2UShields, "P2UShields"},
		{Controls.P2UWeapons, "P2UWeapons"},
		{Controls.P2USpeed, "P2USpeed"},

	};
	private const string FILEPATH = "GameSettings.json";

	#region IO - save and load game settings
	public void SaveToJSON()
	{
		GameSettingsData data = new GameSettingsData() {
			PlayerNames = this.PlayerNames,
			SoundVolume = this.SoundVolume,
			InputEvents = this.InputEvents,
		};

		FileJSON.SaveToJSON(FILEPATH, data);
	}

	public void LoadFromJSON()
	{
		if (! System.IO.File.Exists(FILEPATH))
			return;
		GameSettingsData data = FileJSON.LoadFromJSON<GameSettingsData>(FILEPATH);
		this.PlayerNames = data.PlayerNames;
		this.SoundVolume = data.SoundVolume;
		this.InputEvents = data.InputEvents;

		SetControlsAll();
		SetSoundVolumeAll();
	}
	#endregion
	#region Player
	public void UpdatePlayerName(int player, string newName)
	{
		PlayerNames[player] = newName;
	}
	#endregion
	#region Sound
	private void SetSoundVolumeAll()
	{
		foreach (string bus in SoundVolume.Keys.ToList()){ // Convert to a list to loop through as we change values while looping
			float value = SoundVolume[bus];
			UpdateSoundVolumeSingle(bus, value);
		}
	}

	public void UpdateSoundVolumeSingle(string bus, float value)
	{
		AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex(bus), value-88);
		AudioServer.SetBusMute(AudioServer.GetBusIndex(bus), false);
		if (value == 0){
			AudioServer.SetBusMute(AudioServer.GetBusIndex(bus), true);
		}
		SoundVolume[bus] = value;
	}
	#endregion
	#region Input       

	public void UpdateInputEvent(Controls ctrl, InputEvent ev)
	{
		string action = InputActions[ctrl];
		// GD.Print(action);
		InputMap.EraseAction(action);
		InputMap.AddAction(action);
		
		// if (ev is InputEventJoypadMotion evAxis)
		// {
		//     InputMap.ActionAddEvent(action, evAxis);
		//     GD.Print(evAxis.AsText());
		// }
		// else
		// {
		InputMap.ActionAddEvent(action, ev);
		// }
		
		InputEvents[ctrl] = ev.AsText();
	}

	// Needs cleanup. added joypad support..
	private void SetControlsAll()
	{
		foreach (GameSettings.Controls ctrl in InputActions.Keys){

			// InputEvent key;
			InputEventKey key = new InputEventKey() {Scancode = (uint)OS.FindScancodeFromString(InputEvents[ctrl])};
			// if (key.)
			// I think scancode 0 means invalid key
			// GD.Print("KEY SCANCODE IS: ", key.Scancode);
			if (key.Scancode == 0)
			{

				// get the first word from the inputevent string so we know what type it is
				string evStr = InputEvents[ctrl];
				int index = evStr.IndexOf(" ");
				if (index > 0)
				{
					evStr = evStr.Substring(0, index);
				}

				if (evStr == "InputEventJoypadMotion")
				{
					// GD.Print("The event type is: ", evStr);

					// GD.Print(InputEvents[ctrl]); //^-?\d+(\.\d+?)?$
					MatchCollection evData = Regex.Matches(InputEvents[ctrl], @"-?\d+(\.\d+?)?", RegexOptions.Compiled);
					int axis = int.Parse(evData[0].Value);
					float axisValue = float.Parse(evData[1].Value);
					
					// only allows -1 or 1.. can change this if game takes advantage of varying pressure of the axis
					// GD.Print("AXIS FLOAT VALUE", axisValue);
					if (axisValue < 0)
					{
						axisValue = -1;
					}
					else
					{
						axisValue = 1;
					}

					// GD.Print("AXIS ", axis);
					// GD.Print("AXIS VALUE", axisValue);

					InputEventJoypadMotion evAxis = new InputEventJoypadMotion() { Axis = axis, AxisValue =  axisValue };
					UpdateInputEvent(ctrl, evAxis);
				}
				else if (evStr == "InputEventJoypadButton")
				{
					// GD.Print("The event type is: ", evStr);
					// GD.Print(InputEvents[ctrl]); //^-?\d+(\.\d+?)?$
					MatchCollection evData = Regex.Matches(InputEvents[ctrl], @"-?\d+", RegexOptions.Compiled);
					InputEventJoypadButton evBtn = new InputEventJoypadButton() { ButtonIndex = int.Parse(evData[0].Value) };
					UpdateInputEvent(ctrl, evBtn);
				}
				// GD.Print(evAxis.Axis);
				// GD.Print(evAxis.AxisValue);
				// InputEvent ev = (InputEvent)InputEvents[ctrl];
				// GD.Print("ASD");var arr = Regex.Matches(strText, @"\b[A-Za-z-']+\b")
	// .Cast<Match>()
	// .Select(m => m.Value)
	// .ToArray();
				// GD.Print()
			}
			else
			{
			// InputEvent ev = (InputEvent)InputEvents[ctrl];
				UpdateInputEvent(ctrl, key);
			}
		}
	}
	#endregion
}
