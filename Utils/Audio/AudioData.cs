// Audio data: contains references to all audio files in the project for convenience
// Contains method for loading required sounds - should load all sounds required for a stage at the start.
using System;
using System.Collections.Generic;
using Godot;

public static class AudioData
{

	public enum SoundBus {
		Voice,
		Effects,
		Music,
		Master
	}

	public static Dictionary<SoundBus, string> SoundBusStrings = new Dictionary<SoundBus, string>()
	{
		{SoundBus.Voice, "Voice"},
		{SoundBus.Effects, "Effects"},
		{SoundBus.Music, "Music"},
		{SoundBus.Master, "Master"}
	};


	public enum ButtonSounds {
		ClickStart
	};


	public enum MainMenuSounds {
		Music,
		Intro
	};
	public enum WorldSounds {
		Music,
		Lose,
		CompletedLevel,
		EndIntroNarration,
		EndLevel1Narration,
		EndLevel2Narration,
		EndLevel3Narration,
		WinGameNarration
	};

	public enum WormSound {
		Movement,
		InfestFailed,
		InfestSuccess,
		PowerupPickup,
		HintPickup,
		InfestPerfectHost,
		InfestContaminatedHost,
		LeaveHost
	}

	public enum HostSound {
		Footsteps,
		Suspicious1,
		Suspicious2,
		Suspicious3,
		CaughtParasite1,
		CaughtParasite2,
		CaughtParasite3,
		Fire,
		Explode,
		IdleHum,
		LostParasite1,
		LostParasite2,
		LostParasite3,
		Flee1,
		Flee2,
		Flee3

	}


	public static Dictionary<ButtonSounds, string> ButtonSoundPaths = new Dictionary<ButtonSounds, string>()
	{
		{ButtonSounds.ClickStart, "res://Interface/Sounds/Click.wav"}
	};

	public static Dictionary<MainMenuSounds, string> MainMenuSoundPaths = new Dictionary<MainMenuSounds, string>()
	{
		{MainMenuSounds.Music, "res://Stages/MainMenu/Music/AlienIntro.ogg"},
		{MainMenuSounds.Intro, "res://Stages/MainMenu/Sound/Intro.wav"}
	};

	public static Dictionary<WorldSounds, string> WorldSoundPaths = new Dictionary<WorldSounds, string>()
	{
		{WorldSounds.Music, "res://Stages/World/Music/AlienHoliday.ogg"},
		{WorldSounds.Lose, "res://Stages/World/Music/LoseSoundShort.wav"},
		{WorldSounds.CompletedLevel, "res://Stages/World/Music/TriumphSFX.wav"},
		{WorldSounds.EndIntroNarration, "res://Stages/World/Voice/Intro.wav"},
		{WorldSounds.EndLevel1Narration, "res://Stages/World/Voice/Level1.wav"},
		{WorldSounds.EndLevel2Narration, "res://Stages/World/Voice/Level2.wav"},
		{WorldSounds.EndLevel3Narration, "res://Stages/World/Voice/Level3.wav"},
		{WorldSounds.WinGameNarration, "res://Stages/World/Voice/Win.wav"}
	};

	public static Dictionary<WormSound, string> WormSoundPaths = new Dictionary<WormSound, string>()
	{
		{WormSound.Movement,  "res://Actors/Parasite/Sound/WalkSound2.wav"},
		{WormSound.InfestFailed,  "res://Actors/Parasite/Sound/Infestation_failed.wav"},
		{WormSound.InfestSuccess,  "res://Actors/Parasite/Sound/Infestation_success.wav"},
		{WormSound.InfestContaminatedHost,  "res://Actors/Parasite/Sound/Pain.wav"},
		{WormSound.InfestPerfectHost,  "res://Actors/Parasite/Sound/PerfectInfestation_success.wav"},
		{WormSound.HintPickup,  "res://Actors/Parasite/Sound/Paper.wav"},
		{WormSound.PowerupPickup,  "res://Actors/Parasite/Sound/PowerUpPickup.wav"},
		{WormSound.LeaveHost,  "res://Actors/Parasite/Sound/LeaveHost.wav"}
	};

	public static Dictionary<HostSound, string> HostSoundPaths = new Dictionary<HostSound, string>()
	{
		{HostSound.CaughtParasite1,  "res://Actors/Host/Sound/CaughtPara2.wav"},
		{HostSound.CaughtParasite2,  "res://Actors/Host/Sound/CaughtPara3.wav"},
		{HostSound.CaughtParasite3,  "res://Actors/Host/Sound/CaughtPara4.wav"},
		{HostSound.Footsteps,  "res://Actors/Host/Sound/walkgrass.wav"},
		{HostSound.LostParasite1,  "res://Actors/Host/Sound/LostPara1.wav"},
		{HostSound.LostParasite2,  "res://Actors/Host/Sound/LostPara2.wav"},
		{HostSound.LostParasite3,  "res://Actors/Host/Sound/LostPara3.wav"},
		{HostSound.Suspicious1,  "res://Actors/Host/Sound/Suspicious1.wav"},
		{HostSound.Suspicious2,  "res://Actors/Host/Sound/Suspicious2.wav"},
		{HostSound.Suspicious3,  "res://Actors/Host/Sound/Suspicious3.wav"},
		{HostSound.Fire, "res://Actors/Host/Sound/FireballImpact.wav"},
		{HostSound.IdleHum, "res://Actors/Host/Sound/IdleHum.wav"},
		{HostSound.Flee1, "res://Actors/Host/Sound/Runaway1.wav"},
		{HostSound.Flee2, "res://Actors/Host/Sound/Runaway2.wav"},
		{HostSound.Flee3, "res://Actors/Host/Sound/Runaway3.wav"},
		{HostSound.Explode, "res://Actors/Host/Sound/HostExplode.wav"}
	};


	// Return a dictionary containing loaded sounds. Useful to do when loading/instancing a scene e.g. player.
	// Usage: Dictionary<AudioData.PlayerSounds, AudioStream> playerSounds = 
	//  AudioData.LoadedSounds<AudioData.PlayerSounds>(AudioData.PlayerSoundPaths);
	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);
	// Change to AudioStreamPlayer for music/global sounds and AudioStreamPlayer3D if working in 3D
	public static Dictionary<T, AudioStream> LoadedSounds<T>(Dictionary<T, string> soundPathsDict)
	{
		Dictionary<T, AudioStream> loadedSoundsDict = new Dictionary<T, AudioStream>();
		foreach (T key in soundPathsDict.Keys)
		{
			loadedSoundsDict[key] = (AudioStream) GD.Load(soundPathsDict[key]);
		}
		return loadedSoundsDict;
	}


}
