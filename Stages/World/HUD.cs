using Godot;
using System;

public class HUD : CanvasLayer
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private Panel _pnlMenu;
	private PnlJournal _pnlJournal;
	private PnlSettings _pnlSettings;
	private AnimationPlayer _HUDAnim;
	private Label _noteText;
	private Panel _pnlNote;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_pnlMenu = GetNode<Panel>("PnlMenu");
		_pnlJournal = GetNode<PnlJournal>("PnlJournal");
		_pnlSettings = GetNode<PnlSettings>("PnlSettings");
		_HUDAnim = GetNode<AnimationPlayer>("HUDAnim");
		_noteText = GetNode<Label>("PnlNote/LblText");
		_pnlNote = GetNode<Panel>("PnlNote");
		_pnlMenu.Visible = false;
		_pnlJournal.Visible = false;
		
		if (OS.HasFeature("HTML5"))
		{
			_pnlSettings.GetNode<CheckBox>("CntPanels/PnlGraphics/CBoxFullScreen").Visible = false;
			_pnlSettings.GetNode<Label>("CntPanels/PnlGraphics/LblTitle").Text = "Not available in HTML5 version.";
		}
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
	public override void _Input(InputEvent @event)
	{
		base._Input(@event);
		if (_pnlSettings.Visible)
		{
			return;
		}
		if (Input.IsActionJustReleased("ui_cancel"))
		{
			if (_pnlJournal.Visible)
			{
				return;
			}
			if (_pnlNote.Visible)
			{
				OnBtnNoteClosePressed();
				return;
			}
			if (_pnlMenu.Visible)
			{
				OnBtnReturnPressed();
			}
			else
			{
				OnBtnMenuPressed();		
			}
		}
		if (Input.IsActionJustReleased("P1Attack") || Input.IsActionJustReleased("P2Attack"))
		{
			if (_pnlNote.Visible)
			{
				OnBtnNoteClosePressed();
				return;
			}
		}

		if (Input.IsActionJustReleased("Journal"))
		{
			if (_pnlMenu.Visible || _pnlNote.Visible)
			{
				return;
			}
			if (_pnlJournal.Visible)
			{
				OnBtnJournalClosePressed();
			}
			else
			{
				OnBtnJournalPressed();
			}
		}
	}


	private void OnBtnJournalClosePressed()
	{
		_HUDAnim.Play("HideJournal");
		GetTree().Paused = false;
		TogglePanelButtonsDisable(false);
	}


	// private void OnBtnConfirmCloseSettingsPressed()
	// {
	// 	_HUDAnim.Play("HideSettings");
	// 	GetTree().Paused = false;
	// 	TogglePanelButtonsDisable(false);
	// }

	private void OnBtnMenuPressed()
	{
		_HUDAnim.Play("ShowMenu");	
		GetTree().Paused = true;
		TogglePanelButtonsDisable(true);		
	}
	private void OnBtnReturnPressed()
	{
		_HUDAnim.Play("HideMenu");
		GetTree().Paused = false;
		TogglePanelButtonsDisable(false);
	}
	private void OnBtnJournalPressed()
	{
		_HUDAnim.Play("ShowJournal");
		GetTree().Paused = true;
		TogglePanelButtonsDisable(true);
	}
	public void ShowNote(string text)
	{
		_noteText.Text = text;
		_pnlJournal.AddText(text);
		_HUDAnim.Play("ShowNote");
		GetTree().Paused = true;
		TogglePanelButtonsDisable(true);
	}
	private void OnBtnNoteClosePressed()
	{
		_HUDAnim.Play("HideNote");
		GetTree().Paused = false;
		TogglePanelButtonsDisable(false);
	}

	private void OnBtnSettingsPressed()
	{
		_pnlSettings.Visible = true;
		// _HUDAnim.Play("ShowSettings");
		// GetTree().Paused = true;
		// TogglePanelButtonsDisable(true);
	}

	private void TogglePanelButtonsDisable(bool disabled)
	{
		GetNode<Button>("Panel/BtnMenu").Disabled = disabled;
		GetNode<Button>("Panel/BtnJournal").Disabled = disabled;
	}
	private void OnHUDAnimAnimationFinished(String anim_name)
	{
		// if (anim_name == "ShowMenu")
		// {
		// 	GetTree().Paused = true;
		// }
		// else if (anim_name == "HideMenu")
		// {
		// 	GetTree().Paused = false;
		// }
	}
}

