using Godot;
using System;
using System.Collections.Generic;

public class StageWorld : Stage
{
	public Dictionary<AudioData.WorldSounds, AudioStream> WorldSoundPaths {get; set;} = AudioData.LoadedSounds<AudioData.WorldSounds>(AudioData.WorldSoundPaths);
	private AudioStreamPlayer _musicPlayer {get; set;}
	public enum Level {Tutorial, Horizons, Swarm, PlainSight, End} 
	private Dictionary<Level, string> _levels;
	private LevelScene _currentLevelScene;
	private Level _currentLevel;
	private LblHint _lblHint;
	private PictureStory _pictureStory;
	private HUD _HUD;
	private MiniMap _miniMap;
	private Parasite _parasite;
	private enum TextEvent {Intro, StealthPowerUp, SpeedPowerUp, MaxStealth, MaxSpeed, HintNote}
	private Dictionary<TextEvent, string> _hintTexts = new Dictionary<TextEvent, string>() {
		{TextEvent.Intro, "Quick! Get moving! Slither around using the movement keys. (Default: WASD)"},
		{TextEvent.StealthPowerUp, "Sneaky! A stealth power! Hosts will not be as suspicious."},
		{TextEvent.SpeedPowerUp, "The vapours of this world are SO GOOD! I slither ever faster..."},
		{TextEvent.MaxStealth, "SO sneaky! Can't get any sneakier!!"},
		{TextEvent.MaxSpeed, "It won't work! I'm already the fastest parasite alive!!"},
		{TextEvent.HintNote, "Curious. Perhaps left behind by one of my nomadic brethren...?"}
	};
	public override void _Ready()
	{
		base._Ready();
		_musicPlayer = GetNode<AudioStreamPlayer>("MusicPlayer");
		AudioHandler.PlaySound(_musicPlayer, WorldSoundPaths[AudioData.WorldSounds.Music], AudioData.SoundBus.Music);
		_lblHint = GetNode<LblHint>("HUD/Panel/LblHint");
		_pictureStory = GetNode<PictureStory>("HUD/PictureStory");
		_HUD = GetNode<HUD>("HUD");
		_miniMap = GetNode<MiniMap>("HUD/MiniMap");

		_levels = new Dictionary<Level, string>()
		{
			{Level.Tutorial, "res://Levels/LvlTutorial.tscn"},
			{Level.Horizons, "res://Levels/LvlHorizons.tscn"},
			{Level.Swarm, "res://Levels/LvlSwarm.tscn"},
			{Level.PlainSight, "res://Levels/LvlInPlainSight.tscn"}
		};

		if (SharedData != null)
		{
			if (SharedData.ContainsKey("Level"))
			{
				if ((StageWorld.Level)SharedData["Level"] == Level.End)
				{
					OnFinalPerfectHostFound();
					return;
				}
				else
				{
					LoadLevel((StageWorld.Level)SharedData["Level"]);
					return;
				}
			}
			
		}
		LoadLevel(Level.Tutorial); // the first level goes here

	}

	private void LoadLevel(Level l)
	{
		_currentLevel = l;
		LevelScene level = (LevelScene) ((PackedScene)GD.Load(_levels[l])).Instance();
		level.Name = "Level";
		AddChild(level);
		_currentLevelScene = level;

		foreach (Host host in GetNode<Node2D>("Level/YSort/CntHosts").GetChildren())
		{
			host.Connect(nameof(Host.ParasiteCaught), this, nameof(OnParasiteCaught));
			host.Connect(nameof(Host.Died), this, nameof(OnHostDied));
			host.Connect(nameof(Host.LeftHost), this, nameof(OnLeftHost));
			host.Connect(nameof(Host.PerfectHostFound), this, nameof(OnPerfectHostFound));
			host.Connect(nameof(Host.ContaminatedHostEntered), this, nameof(OnContaminatedHostEntered));
			host.Connect(nameof(Host.BecameSuspicious), this, nameof(OnHostBecameSuspicious));
		}
		foreach (TextEventTrigger tEv in GetNode<Node2D>("Level/YSort/CntTextEventTriggers").GetChildren())
		{
			tEv.Connect("body_entered", this, nameof(OnTextEventTriggerEntered), new Godot.Collections.Array(){tEv.Text});
		}
		foreach (PowerUp powerUp in GetNode<Node2D>("Level/YSort/CntPowerUps").GetChildren())
		{
			powerUp.Connect(nameof(PowerUp.PowerUpActivated), this, nameof(OnPowerUpActivated));
		}
		foreach (HintNote note in GetNode<Node2D>("Level/YSort/CntHintNotes").GetChildren())
		{
			note.Connect(nameof(HintNote.HintNoteActivated), this, nameof(OnHintNoteActivated));
		}
		_parasite = GetNode<Parasite>("Level/YSort/Parasite");

		if (l == Level.Tutorial)
		{
			_lblHint.SetHintText(_hintTexts[TextEvent.Intro]);
		}

		_miniMap.ParasiteNodePath = _parasite.GetPath();
		GetNode<Label>("HUD/PnlLvlTitle/LblTitle").Text = level.HumanReadableName;
		_miniMap.Init();
	}

	public void OnHostBecameSuspicious(Vector2 position)
	{
		foreach (Host h in GetNode<Node2D>("Level/YSort/CntHosts").GetChildren())
		{
			if (h.IsSuspicious)
			{
				continue;
			}
			if (h.HostControlState is AIHostControlState hostAIControlState)
			{
				if (hostAIControlState.GetActionState() == AIHostControlState.AIState.Fleeing) // they shouldnt suspect if they are fleeing
				{
					continue;
				}
			}
			if (h.Position.DistanceTo(position) < 400)
			{
				// GD.Print(h.Position.DistanceTo(position));
				h.OnNearbyHostSuspicious(_parasite.Position);
			}
		}
	}

	//// TESTING ONLY
	// public override void _Input(InputEvent ev) /// TESTING ONLY
	// {
	// 	base._Input(ev);

	// 	if (ev.IsActionPressed("ui_home"))
	// 	{
	// 		OnPerfectHostFound();
	// 	}
	// }
	//// TESTING ONLY

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		if (!_parasite.Visible)
		{
			foreach (Host host in GetNode<Node2D>("Level/YSort/CntHosts").GetChildren())
			{
				if (host.GetControlState() == Host.ControlState.PlayerState)
				{
					_parasite.Position = host.Position;
					return;
				}
			}
		}
	}

	

	public void SaveCompletedLevels()
	{
		LevelData data = new LevelData();
		
		List<int> completedLevels = GetLevelsCompleted();

		if (completedLevels == null)
		{
			completedLevels = new List<int>();
		}
		if (!completedLevels.Contains((int)_currentLevelScene.NextLevel))
		{
			completedLevels.Add((int)_currentLevelScene.NextLevel);
		}
		data.LevelsCompleted = completedLevels;

		FileJSON.SaveToJSON(OS.GetUserDataDir() + "/CompletedLevels.json", data);
	}

	private List<int> GetLevelsCompleted()
	{
		string path = OS.GetUserDataDir() + "/CompletedLevels.json";
		if (! System.IO.File.Exists(path))
		{
			GD.Print("File at " + path + " not found.");
			return null;
		}
		LevelData data = FileJSON.LoadFromJSON<LevelData>(path);
		return data.LevelsCompleted;
	}

	// private void OnFinalEndgame()
	// {
	// 	_pictureStory.Reset();
	// 	_pictureStory.PictureStoryFinished+=this.OnFinalEndGameFinished;
	// 	_pictureStory.StayPausedOnFinish = true;
	// 	_pictureStory.FadeOutOnFinish = false;
	// 	_pictureStory.VisibleOnFinish = true;
	// 	_pictureStory.AddScreen("res://icon.png", "I must notify my parasitic brethren! This world is RIPE for invasion!!");
	// 	_pictureStory.Start();
	// }

	private void OnFinalEndGameFinished()
	{
		OnBtnMainMenuPressed();
	}

	private void OnContaminatedHostEntered()
	{
		AudioHandler.PlaySound(_musicPlayer, WorldSoundPaths[AudioData.WorldSounds.Lose], AudioData.SoundBus.Music);
		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnFinalEndGameFinished;
		_pictureStory.StayPausedOnFinish = true;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		_pictureStory.AddScreen("res://Stages/World/Art/PlaceholderLosePoison.png", "SABOTAGE!! This poison host has destroyed me!");
		_pictureStory.Start();
	}

	private void OnPerfectHostFound()
	{
		AudioHandler.PlaySound(_musicPlayer, WorldSoundPaths[AudioData.WorldSounds.CompletedLevel], AudioData.SoundBus.Music);
		if (_currentLevelScene.NextLevel == Level.End)
		{
			OnFinalPerfectHostFound();
			return;
		}

		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnLevelCompleted;
		_pictureStory.StayPausedOnFinish = true;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		_pictureStory.AddScreen(_currentLevelScene.EndLevelPicturePath, _currentLevelScene.EndLevelText, WorldSoundPaths[_currentLevelScene.EndLevelNarration]);// "Well, this host is stronger than most.. onto the next!");
		_pictureStory.Start();

	}

	private void OnFinalPerfectHostFound()
	{
		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnFinalLevelCompleted;
		_pictureStory.StayPausedOnFinish = true;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		_pictureStory.AddScreen(_currentLevelScene.EndLevelPicturePath, _currentLevelScene.EndLevelText, WorldSoundPaths[_currentLevelScene.EndLevelNarration]);
		_pictureStory.Start();
	}

	public void OnParasiteCaught()
	{
		AudioHandler.PlaySound(_musicPlayer, WorldSoundPaths[AudioData.WorldSounds.Lose], AudioData.SoundBus.Music);
		_parasite.OnCaught();

		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnFinalEndGameFinished;
		_pictureStory.StayPausedOnFinish = true;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		_pictureStory.AddScreen("res://Stages/World/Art/LoseSquish.png", "Arrrgh! Death!\n*SQUISH* *BURN*");
		_pictureStory.Start();
	}

	private void OnLevelCompleted()
	{		
		
		SaveCompletedLevels();
		//OnBtnMainMenuPressed();
		SceneManager.SimpleChangeScene(SceneData.Stage.World, new Dictionary<string, object>() {
			{"Level", _currentLevelScene.NextLevel}
		});
	}

	private void OnFinalLevelCompleted()
	{
		OnBtnMainMenuPressed();
	}

	private void OnHintNoteActivated(string text, HintNote hintNote)
	{
		_miniMap.RemoveMarker(hintNote);
		_lblHint.SetHintText(_hintTexts[TextEvent.HintNote]);
		_HUD.ShowNote(text);
		_parasite.OnHintNoteActivated();
	}

	private void OnPowerUpActivated(PowerUp.PowerUpType powerType, PowerUp powerUp)
	{

		_parasite.OnPowerupActivated();
		_miniMap.RemoveMarker(powerUp);
		if (powerType == PowerUp.PowerUpType.Speed)
		{
			if (_parasite.Speed >= _parasite.MaxSpeed)
			{
				_lblHint.SetHintText(_hintTexts[TextEvent.MaxSpeed]);
				return;
			}
			_parasite.OnSpeedPowerUp(); // TODO - parasite needed // _parasite.Speed = Mathf.Clamp(_parasite.Speed+50f, 400f, 800f);
			_lblHint.SetHintText(_hintTexts[TextEvent.SpeedPowerUp]);
		}
		else if (powerType == PowerUp.PowerUpType.Stealth)
		{
			foreach (Host host in GetNode<Node2D>("Level/YSort/CntHosts").GetChildren())
			{
				if (host.StealthModifier <= 0.3f)
				{
					_lblHint.SetHintText(_hintTexts[TextEvent.MaxStealth]);
					return;
				}
				host.OnStealthPowerUp();
			}
			_lblHint.SetHintText(_hintTexts[TextEvent.StealthPowerUp]);
		}
	}

	public void OnHostDied(Vector2 position, Host host)
	{
		_parasite.OnHostDied(position);
		_miniMap.RemoveMarker(host);
		foreach (Host h in GetNode<Node2D>("Level/YSort/CntHosts").GetChildren())
		{
			if (h.Position.DistanceTo(position) < 500)
			{
				// GD.Print(h.Position.DistanceTo(position));
				h.Flee(position);
			}
		}
	}

	public void OnLeftHost(Vector2 position)
	{
		_parasite.OnHostDied(position);
		// GetNode<Camera2D>("YSort/Parasite/Cam").Current = true; presumably this is done in the parasite code...
	}

	public void OnTextEventTriggerEntered(PhysicsBody2D body, string text)
	{
		if (body is Parasite parasite)
		{
			_lblHint.SetHintText(text);
		}
	}
	

	private void OnBtnMainMenuPressed()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.MainMenu);
	}



}



