// Stage menu: simple menu script connecting local and networked game signals to our scene manager.
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class StageMainMenu : Stage
{
	private AudioStreamPlayer _musicPlayer;
	private AudioStreamPlayer _soundPlayer;
	private Dictionary<AudioData.MainMenuSounds, AudioStream> _mainMenuSounds = AudioData.LoadedSounds<AudioData.MainMenuSounds>(AudioData.MainMenuSoundPaths);
	private PictureStory _pictureStory;
	private Panel _popAbout;
	private PnlSettings _pnlSettings;
	private Panel _pnlLevels;

	private Dictionary<StageWorld.Level, Button> _levelButtons;
	private Button _firstLevelButton;
	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);

	public override void _Ready()
	{
		base._Ready();
		// SetBtnToggleFullScreenText();
		_popAbout = GetNode<Panel>("PopAbout");
		_musicPlayer = GetNode<AudioStreamPlayer>("MusicPlayer");
		_soundPlayer = GetNode<AudioStreamPlayer>("SoundPlayer");
		_pictureStory = GetNode<PictureStory>("PictureStory");
		_pnlLevels = GetNode<Panel>("PnlLevels");
		_pnlSettings = GetNode<PnlSettings>("PnlSettings");
		_pnlSettings.Visible = false;
		_pnlLevels.Visible = false;

		_levelButtons = new Dictionary<StageWorld.Level, Button>()
		{
			// {StageWorld.Level.Tutorial, GetNode<Button>("PnlLevels/VBoxContainer/BtnLvlTutorial")},
			{StageWorld.Level.Horizons, GetNode<Button>("PnlLevels/VBoxContainer/BtnLvlHorizons")},
			{StageWorld.Level.Swarm, GetNode<Button>("PnlLevels/VBoxContainer/BtnLvlSwarm")},
			{StageWorld.Level.PlainSight, GetNode<Button>("PnlLevels/VBoxContainer/BtnLvlPlainSight")}
		};
		_firstLevelButton = GetNode<Button>("PnlLevels/VBoxContainer/BtnLvlTutorial");

		if (OS.HasFeature("HTML5"))
		{
			GetNode<Button>("HBoxContainer/BtnQuit1").Visible = false;
			GetNode<Button>("HBoxContainer/BtnToggleMute").Visible = true;
			GetNode<Button>("HBoxContainer/BtnToggleFullScreen").Visible = false;
			_pnlSettings.GetNode<CheckBox>("CntPanels/PnlGraphics/CBoxFullScreen").Visible = false;
			_pnlSettings.GetNode<Label>("CntPanels/PnlGraphics/LblTitle").Text = "Not available in HTML5 version.";
		}
		
		AudioHandler.PlaySound(_musicPlayer, _mainMenuSounds[AudioData.MainMenuSounds.Music], AudioData.SoundBus.Music);

		// if (SharedData != null)
		// {
		// 	if (SharedData.ContainsKey("Fullscreen"))
		// 	{
		// 		OS.WindowFullscreen = (bool) SharedData["Fullscreen"];
		// 	}
		// }
		// else
		// {
		// 	if (!OS.HasFeature("HTML5"))
		// 	{
		// 		_on_BtnToggleFullScreen_pressed();
		// 	}
		// }
		
		_popAbout.Visible = false;
		// Load settings at start if they exist. This should run at startup so if another scene is set at startup put this there.
		// GameSettings.Instance.LoadFromJSON();
		GetNode<Button>("HBoxContainer/BtnToggleMute").Text = AudioServer.IsBusMute(AudioServer.GetBusIndex("Master")) ? "UNMUTE" : "MUTE";
		
		//GetNode<AnimatedSprite>("AnimatedSprite").Play();

		_firstLevelButton.Disabled = false;
		_firstLevelButton.Connect("pressed", this, nameof(OnLevelButtonPressed), new Godot.Collections.Array() {StageWorld.Level.Tutorial});
		List<int> levelsCompleted = GetLevelsCompleted();
		if (levelsCompleted != null)
		{
			foreach (StageWorld.Level l in _levelButtons.Keys)
			{
				if (levelsCompleted.Contains((int)l))
				{
					_levelButtons[l].Disabled = false;
					_levelButtons[l].Connect("pressed", this, nameof(OnLevelButtonPressed), new Godot.Collections.Array() {l});
				}
			}
		}
	}

	private void OnBtnSelectLevelPressed()
	{
		_pnlLevels.Visible = true;
	}

	private void OnLevelButtonPressed(StageWorld.Level l)
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.World, new Dictionary<string, object>() {
			{"Level", l}
		});
	}

	// public async void ScoreTest()
	// {
	// 	Node silentWolf = GetNode("/root/SilentWolf");
	// 	silentWolf.GetNode("Scores").Call("get_high_scores");
	// 	await ToSignal(silentWolf.GetNode("Scores"), "sw_scores_received");
	// 	string scores = (string)silentWolf.GetNode("Scores").Call("get_stored_scores");
	// 	GD.Print(scores);
	// }

	private List<int> GetLevelsCompleted()
	{
		string path = OS.GetUserDataDir() + "/CompletedLevels.json";
		if (! System.IO.File.Exists(path))
		{
			GD.Print("File at " + path + " not found.");
			return null;
		}
		LevelData data = FileJSON.LoadFromJSON<LevelData>(path);
		return data.LevelsCompleted;
	}

	private void Intro() // this is the new game intro
	{
		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnIntroFinished;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		_pictureStory.AddScreen("res://Stages/World/Art/Intro.png", 
		@"Noddy was no ordinary parasite. He loved his nomadic life - infesting and controlling the minds of human after human... but today, Noddy had a problem. Every time he infested a human, they soon died! So, he began his quest to find the fabled perfect host.", _mainMenuSounds[AudioData.MainMenuSounds.Intro]);
		// _pictureStory.OverrideColor(new Color(0,0,0), new Color(1,1,1));
		_pictureStory.Start();
		
	}
	//@"My parasitic brethren and I must forever travel the stars in search of new hosts. What manner of planet is this, teeming with life?"

	private void OnIntroFinished()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.World);
	}

	// private void Continue()
	// {
	// 	if (GetLevelsCompleted() == null)
	// 	{
	// 		GD.Print("Error, continue should be greyed out");
	// 		return;
	// 	}
	// 	StageWorld.Level level = (StageWorld.Level) GetLevelsCompleted().Max();
	// 	SceneManager.SimpleChangeScene(SceneData.Stage.World, new Dictionary<string, object>() {
	// 		{"Level", level},
	// 		{"CompletedLevels", GetLevelsCompleted()}
	// 	});
	// }

	private void OnBtnPlayPressed()
	{
		Intro();
	}

	private void OnBtnAboutPressed()
	{
		GetNode<Panel>("PopAbout").Visible = true;
		GetNode<Control>("PopAbout/CntAbout").Visible = true;
	}

	private void OnBtnBackPressed()
	{
		GetNode<Panel>("PopAbout").Visible = false;
	}

	public override void _Input(InputEvent ev)
	{
		if (_popAbout.Visible && ev is InputEventMouseButton evMouseButton && ev.IsPressed())
		{
			if (! (evMouseButton.Position.x > _popAbout.RectGlobalPosition.x && evMouseButton.Position.x < _popAbout.RectSize.x + _popAbout.RectGlobalPosition.x
			&& evMouseButton.Position.y > _popAbout.RectGlobalPosition.y && evMouseButton.Position.y < _popAbout.RectSize.y + _popAbout.RectGlobalPosition.y) )
			{
				OnBtnBackPressed();
			}
		}
	}

	private void _on_BtnQuit_pressed()
	{
		GetTree().Quit();
	}

	private void ToggleMute()
	{
		bool muted = AudioServer.IsBusMute(AudioServer.GetBusIndex("Master"));
		AudioServer.SetBusMute(AudioServer.GetBusIndex("Master"), !muted);
		string label = muted ? "MUTE" : "UNMUTE";
		GetNode<Button>("HBoxContainer/BtnToggleMute").Text = label;
	}

	private void OnGameLinkButtonPressed()
	{
		OS.ShellOpen("https://sage7.itch.io/");
	}


	private void OnBtnScoresPressed()
	{
		_pnlSettings.Visible = true;
	}


	private void _on_BtnToggleFullScreen_pressed()
	{
		// OS.WindowFullscreen = !OS.WindowFullscreen;
		// SetBtnToggleFullScreenText();
		
	}

	private void SetBtnToggleFullScreenText()
	{
		GetNode<Button>("HBoxContainer/BtnToggleFullScreen").Text = OS.WindowFullscreen == true ? "WINDOWED" : "FULLSCREEN";
	}

	private void OnBtnTexBackgroundPressed()
	{
		_pnlSettings.OnBtnClosePressed();
		_popAbout.Visible = false;
		_pnlLevels.Visible = false;
		GD.Print("test");
	}

	private void OnBtnPnlLevelsClosePressed()
	{
		_pnlLevels.Visible = false;
	}

}


