using Godot;
using System;
using System.Collections.Generic;

public class ObstacleBush : StaticBody2D
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private Random _rand = new Random();
	private List<Rect2> _regionRects = new List<Rect2>()
	{
		new Rect2(new Vector2(20*1f, 20*1f), new Vector2(500*1f, 500*1f)),
		new Rect2(new Vector2(560*1f, 20*1f), new Vector2(500*1f, 500*1f)),
		new Rect2(new Vector2(1100*1f, 20*1f), new Vector2(500*1f, 500*1f)),
	};
	public string MiniMapIcon = "Obstacle";
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GetNode<Sprite>("Sprite").RegionRect = _regionRects[_rand.Next(_regionRects.Count)];
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
