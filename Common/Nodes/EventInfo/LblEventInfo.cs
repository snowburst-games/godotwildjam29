using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class LblEventInfo : Label
{

	// [Export]
	// private float _playbackSpeed = 1;
	private List<string> _queuedTexts = new List<string>();

	private Dictionary<string, float> _textSpeedDict = new Dictionary<string, float>();
	private Dictionary<string, EventColor> _textColourDict = new Dictionary<string, EventColor>();
	private AnimationPlayer _anim;
	public override void _Ready()
	{
		_anim = GetNode<AnimationPlayer>("Anim");
		// _anim.PlaybackSpeed = _playbackSpeed;

		// QueueText("text 1", 1, EventColor.Default); //TESTS
		// QueueText("text 2", 2, EventColor.Red);
		// QueueText("text 5", 5, EventColor.Blue);
		// QueueText("text 3", 3, EventColor.Default);
		// QueueText("text 4", 4, EventColor.Green);
		// QueueText("text 5", 5, EventColor.Blue);
		// QueueText("text 5", 5, EventColor.Blue);
	}

	public enum EventColor
	{
		Default,
		Red,
		Green,
		Blue
	}

	private Dictionary<EventColor, Color> _eventColorDict = new Dictionary<EventColor, Color>
	{
		{EventColor.Default, new Color(1,1,1)},
		{EventColor.Red, new Color(1,0,0)},
		{EventColor.Green, new Color(0,1,0)},
		{EventColor.Blue, new Color(0,0,1)}
	};

	public void QueueText(string text, float speed=5, EventColor color=EventColor.Default) // colour - 0 = normal, 1 = red, 2 = green, 3 = blue
	{
		_textSpeedDict[text] = speed;
		_queuedTexts.Add(text);
		_textColourDict[text] = color;
		// if (!_anim.IsPlaying())
		{
			PlayNextText();
		}
	}

	private void OnAnimAnimationFinished(string animName)
	{
		// _textSpeedDict.Remove(_queuedTexts[0]);
		// _queuedTexts.RemoveAt(0);
		if (_queuedTexts.Count > 0)
		{
			PlayNextText();
		}
	}

	private void PlayNextText()
	{
		Text = _queuedTexts[0];
		_anim.PlaybackSpeed = _textSpeedDict[_queuedTexts[0]];
		Color c = _eventColorDict[_textColourDict[_queuedTexts[0]]];
		// GD.Print(c.)
		//get_node("Label").add_color_override("font_color", Color(1,0,0,1))
		this.AddColorOverride("font_color", c);
		// Modulate = new Color(c.r, c.g, c.b, Modulate.a);
		_anim.Stop();
		_anim.Play("FadeInOut");


		_queuedTexts.Remove(Text);
		foreach (string s in _textSpeedDict.Keys.ToList())
		{
			if (!_queuedTexts.Contains(s))
			{
				_textSpeedDict.Remove(s);
			}
		}
		foreach (string s in _textColourDict.Keys.ToList())
		{
			if (!_queuedTexts.Contains(s))
			{
				_textColourDict.Remove(s);
			}
		}
		// _textSpeedDict.Remove(Text);
	}

}
