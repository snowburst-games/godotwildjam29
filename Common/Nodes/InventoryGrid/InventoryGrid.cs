// Make a sprite dict for each item type and add the unique sprite when rendering depending on the type
// Actually let's just make each grid cell contain an InventoryItem
// Make an InventoryItem class
// Can also include things like height and width
// Then wouldn't need itemdict or spritelist presumably

using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class InventoryGrid : Control
{
	[Export]
	public string Title {get; set;}
	[Export]
	public float BorderSize {get; set;} = 45;

	public delegate void InventoryClosedDelegate(int UID);
	public event InventoryClosedDelegate InventoryClosed;

	public int UID {get; set;} = 0;
	
	private int _numRows = 5;
	private int _numCols = 11;
	public int Capacity {get; private set;} = 50;
	private int _cellSize = 64;
	private int _itemUnitSize = 64;
	private int _lineThickness = 3;
	// private List<Dictionary<int,int>> _grid = new System.Collections.Generic.List<Dictionary<int, int>>();

	// private List<List<int>> _grid = new List<List<int>>();

	private InventoryItem[][] _grid;

	private List<InventoryItem[]> _grid2;
	private InventoryItem[][] _grid3;

	private List<Sprite> _spriteList = new List<Sprite>();
	private Dictionary<int, ItemType> _itemDict = new Dictionary<int, ItemType>() {
		{0, ItemType.Empty},
		{1, ItemType.Turret}
	};

	private GridBG _background;
	private BaseButton _btnClose;

	public enum ItemType { // to make this reusable, make a separate ItemData script with the different item types?
		Empty, Turret, Apple
	}

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{

		_background = GetNode<GridBG>("GridBG");
		_btnClose = GetNode<BaseButton>("BtnClose");
		
		_btnClose.RectScale = new Vector2(BorderSize/100f, BorderSize/100f);

		MakeGrid();

		_background.RenderGrid(_numCols, _numRows, _cellSize, _lineThickness, Title);

		// centers the grid
		RectPosition = new Vector2((GetViewportRect().Size.x - (_numCols * _cellSize))/2f, (GetViewportRect().Size.y - (_numRows * _cellSize))/2f);
		// PrintGrid();
		// Update();
		// SetItem(new int[2] {7,3}, item);
		// PrintGrid();
	}

	// public void MakeGrid()
	// {
	// 	for (int col = 0; col < _numCols; col++)
	// 	{
	// 		// Dictionary
	// 		// _grid.Add(new Dictionary<int, int>());
	// 		_grid.Add(new List<int>());

	// 		for (int row = 0; row < _numRows; row++)
	// 		{
	// 			_grid[col].Add(0);
	// 			// _grid[col][row] = 0; // 0 means empty
	// 		}
	// 	}
	// }

	public override void _PhysicsProcess(float delta)
	{
		// GD.Print(GetGlobalMousePosition());#

		// if (Input.IsActionJustPressed("SetTile"))
		// {
		// 	int[] gridPos = WorldToGrid(GetGlobalMousePosition());
		// 	GD.Print("GridPos: " + gridPos[0] + ", " + gridPos[1]);

		// 	Vector2 worldPos = GridToWorld(gridPos);
		// 	GD.Print("WorldPos: " + worldPos);

		// 	// // SetItem(gridPos, 5);

		// 	// // GetNode<Sprite>("icon").Position = worldPos;

		// 	// GD.Print("Grid position item: ", GetItemFromCoord(gridPos));
		// 	// GD.Print("World position item: ", GetItemFromCoord(worldPos));
		// }

		// if (Input.IsActionJustPressed("ClearTile"))
		// {
		// 	PrintGrid();
		// }
	}

	public InventoryItem GetItemFromCoord(Vector2 worldPos)
	{
		int[] coord = WorldToGrid(worldPos);
		return GetItemFromCoord(coord);//_grid[coord[0]][coord[1]];
	}

	public InventoryItem GetItemFromCoord(int[] coord)
	{
		if (!IsInBounds(coord))
		{
			return null;
		}
		return _grid[coord[0]][coord[1]];
	}

	private bool IsInBounds(int[] coord)
	{
		GD.Print("Here we go..");
		GD.Print(_grid.Length);
		GD.Print(coord[0]);
		if (_grid.Length <= coord[0] || coord[0] < 0)
		{
			return false;
		}

		if (_grid[coord[0]].Length <= coord[1] || coord[1] < 0)
		{
			return false;
		}
		return true;
	}

	public int[] WorldToGrid(Vector2 worldPos)
	{
		int x = Convert.ToInt32((worldPos.x / _cellSize)  - this.RectPosition.x/_cellSize - 0.5);
		int y = Convert.ToInt32((worldPos.y / _cellSize)  - this.RectPosition.y/_cellSize - 0.5);
		// GD.Print("Grid: ", x, ", ", y);
		return new int[2] { x,y};
	}

	public Vector2 GridToWorld(int[] gridPos)
	{
		float x = (gridPos[0] * _cellSize );
		float y = (gridPos[1] * _cellSize);
		// GD.Print("World: ", x, ", ", y);
		return new Vector2(x,y);// + new Vector2(_cellSize/2, _cellSize/2);
	}

	private void SetItem(int[] coord, InventoryItem item) // coord x and y are counted from 0, so {7, 3} would be column 6, row 2.
	{
		_grid[coord[0]][coord[1]] = item;
		RenderItemAtCoord(coord, item); // may be remove?
	}

	private void MakeGrid()
	{
		Capacity = _numRows * _numCols;
		// _grid = new int[_numCols][];
		_grid = new InventoryItem[_numCols][];
		// _grid2 = new List<InventoryItem[]>();
		for (int col = 0; col < _numCols; col++)
		{
			// _grid[col] = new int[_numRows];
			// _grid2.Add(new InventoryItem[_numRows]);
			_grid[col] = new InventoryItem[_numRows];
			// Dictionary
			// _grid.Add(new Dictionary<int, int>());
			// _grid.Add(new List<int>());

			for (int row = 0; row < _numRows; row++)
			{
				// _grid2[col][row] = new InventoryItem();
				// _grid[col].Add(0);
				// _grid[col][row] = 0;
				_grid[col][row] = null;//new InventoryItem();
				// _grid[col][row] = 0; // 0 means empty
			}
		}

		_background.RectSize = new Vector2(_numCols*_cellSize, _numRows*_cellSize);
		_btnClose.RectPosition = new Vector2(_numCols*_cellSize,  -_btnClose.RectSize.y*_btnClose.RectScale.y) ;
			// DrawLine(new Vector2((col)*_cellSize, 0), new Vector2((col)*_cellSize, (_numRows*_cellSize)), new Color(0,0,0,0.1f), _lineThickness, true);
	}

	public List<InventoryItem> GetItemList()
	{
		List<InventoryItem> items = new List<InventoryItem>();

		for (int col = 0; col < _grid.Length; col++)
		{
			for (int row = 0; row < _grid[col].Length; row++)
			{
				if (_grid[col][row] != null) // if non 0 (i.e. not empty)
				{
					items.Add(_grid[col][row]);
				}
			}
		}

		return items;
	}

	public int GetNumberOfItems(InventoryItem.ItemClass itemClass)
	{
		// List<InventoryItem> itemList = GetItemList();
		// int num = 0;
		// foreach (InventoryItem item in itemList)
		// {
		// 	if (item.GetItemClass() == targetItem.GetItemClass())
		// 	{
		// 		num += 1;
		// 	}
		// }
		// return num;

		return GetItemList().FindAll(x => x.GetItemClass() == itemClass).Count();
	}

	public InventoryItem RemoveItem(InventoryItem item)
	{
		InventoryItem itemToRemove;
		for (int col = 0; col < _grid.Length; col++)
		{
			for (int row = 0; row < _grid[col].Length; row++)
			{
				if (_grid[col][row] == item) // if non 0 (i.e. not empty)
				{
					itemToRemove = _grid[col][row];
					// itemToRemove.RectPosition = new Vector2(-1000, -1000); // this is temporary until it is repositioned.
					RemoveChild(itemToRemove);
					_grid[col][row] = null;
					return itemToRemove;					
				}
			}
		}
		return null;
	}

	// public bool RemoveFromGrid(InventoryItem item)
	// {
	// 	for (int col = 0; col < _grid.Length; col++)
	// 	{
	// 		for (int row = 0; row < _grid[col].Length; row++)
	// 		{
	// 			if (_grid[col][row] != null) // if non 0 (i.e. not empty)
	// 			{
	// 				items.Add(_grid[col][row]);
	// 			}
	// 		}
	// 	}
	// }

	public InventoryItem GetItemByID(int itemID)
	{
		foreach (InventoryItem item in GetItemList())
		{
			if (item.ItemID == itemID)
			{
				return item;
			}
		}
		return null;
	}

	public bool AddItem(InventoryItem item)
	{
		if (GetItemList().Count >= Capacity)
		{
			// FULL
			return false;
		}
		AddFirstToGrid(item);
		return true;
	}

	// private void RemoveFirstFromGrid(InventoryItem item)
	// {
	// 	for (int col = 0; col < _grid.Length; col++)
	// 	{
	// 		for (int row = 0; row < _grid[col].Length; row++)
	// 		{
	// 			if (_grid[col][row] == itemID) // if non 0 (i.e. not empty)
	// 			{
	// 				_grid[col][row] = 0;
	// 				// remove the sprite here
	// 				RenderItemAtCoord(new int[2] {col, row}, 0);
	// 				return;
	// 			}
	// 		}
	// 	}
	// }

	// private void RemoveLastFromGrid(int itemID)
	// {
	// 	for (int col = _grid.Length-1; col >= 0; col--)
	// 	{
	// 		for (int row = _grid[col].Length-1; row >= 0; row--)
	// 		{
	// 			if (_grid[col][row] == itemID) // if non 0 (i.e. not empty)
	// 			{
	// 				_grid[col][row] = 0;
	// 				// remove the sprite here
	// 				RenderItemAtCoord(new int[2] {col, row}, 0);
	// 				return;
	// 			}
	// 		}
	// 	}
	// }

	private void AddFirstToGrid(InventoryItem item)
	{
		for (int col = 0; col < _grid.Length; col++)
		{
			for (int row = 0; row < _grid[col].Length; row++)
			{
				if (_grid[col][row] == null) // if non 0 (i.e. not empty)
				{
					SetItem(new int[2] {col, row}, item);
					return;
				}
			}
		}
	}

	private void PrintGrid()
	{
		for (int col = 0; col < _grid.Length; col++)
		{
			GD.Print(col);
			for (int row = 0; row < _grid[col].Length; row++)
			{
				if (_grid[col][row] != null)
				{
					GD.Print(_grid[col][row].ItemID);
					GD.Print(_grid[col][row].Item.ItemName);
				}

			}
		}
	}


	private void RenderItemAtCoord(int[] coord, InventoryItem item) // fix with item ID sprite
	{
		// GD.Print("GRID COORDINATE: ", coord[0], coord[1]);
		Vector2 worldPos = GridToWorld(coord);
		Vector2 correctedWorldPos = new Vector2(worldPos.x + (_cellSize-_itemUnitSize)/2, worldPos.y + (_cellSize-_itemUnitSize)/2);
		// GD.Print("World COORDINATE: ", worldPos.x, worldPos.y);

		AddChild(item);
		item.RectPosition = correctedWorldPos;

		// if (itemID == 0)
		// {
		// 	foreach (Sprite sToRemove in _spriteList.ToList())
		// 	{
		// 		// GD.Print(sToRemove.Position);
		// 		// GD.Print(WorldToGrid(sToRemove.Position)[0], ", ", WorldToGrid(sToRemove.Position)[1]);
		// 		// GD.Print(coord[0], ", ", coord[1]);
		// 		if (sToRemove.Position == worldPos)
		// 		{
		// 			GD.Print("TEST3?");
		// 			_spriteList.Remove(sToRemove);
		// 			sToRemove.QueueFree();
		// 		}
		// 	}
		// 	return;
		// }

		// Sprite s = new Sprite();
		// s.Texture = (Texture)GD.Load("res://icon.png");
		// AddChild(s);
		// s.Position = worldPos;//GridToWorld(coord);
		// _spriteList.Add(s);
	}

	// in future render sprites depending on the item ID
	private void RenderAllGridItems()
	{
		for (int col = 0; col < _grid.Length; col++)
		{
			for (int row = 0; row < _grid[col].Length; row++)
			{
				if (_grid[col][row] != null) // if non 0 (i.e. not empty)
				{
					RenderItemAtCoord(new int[2] {col, row}, _grid[col][row]);
				}
			}
		}
	}


	private void OnBtnClosePressed()
	{
		Visible = false;
		InventoryClosed?.Invoke(this.UID);
	}

}

