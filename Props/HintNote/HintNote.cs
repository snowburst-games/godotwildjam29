using Godot;
using System;
using System.Collections.Generic;

public class HintNote : Area2D
{
	[Signal]
	public delegate void HintNoteActivated(string text, HintNote hintNote);
	[Export]
	private string _noteText = "";

	private AnimationPlayer _anim;
	private bool _activated = false;
	private Random _rand = new Random();
	private List<Rect2> _noteRegionRects = new List<Rect2>()
	{
		{new Rect2(new Vector2(546,20), new Vector2(500,500))},
		{new Rect2(new Vector2(2180/1f,20), new Vector2(500,500))},
		{new Rect2(new Vector2(2720/1f,20), new Vector2(500,500))},
		{new Rect2(new Vector2(3260/1f,20), new Vector2(500,500))},
		{new Rect2(new Vector2(3800/1f,20), new Vector2(500,500))}
	};

	public override void _Ready()
	{
		_anim = GetNode<AnimationPlayer>("Anim");	
		GetNode<Sprite>("Sprite").RegionRect = 	_noteRegionRects[_rand.Next(_noteRegionRects.Count)];
	}

	private void OnHintNoteBodyEntered(PhysicsBody2D body)
	{
		if (_activated)
		{
			return;
		}
		if (body is Parasite p)
		{
			EmitSignal(nameof(HintNoteActivated), _noteText, this);
			_anim.Play("Die");
			_activated = true;
		}
	}

	private void OnAnimAnimationFinished(String anim_name)
	{
		if (anim_name == "Die")
		{
			QueueFree();
		}
	}
}
