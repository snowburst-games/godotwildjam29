using Godot;
using System;

public class TextEventTrigger : Area2D
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	[Export]
	public string Text {get; set;}

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}


	private void OnTextEventTriggerBodyEntered(PhysicsBody2D body)
	{
		if (body is Parasite p)
		{
			QueueFree();
		}
	}

}
