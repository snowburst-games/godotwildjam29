using Godot;
using System;
using System.Collections.Generic;

public class PowerUp : Area2D
{
	[Signal]
	public delegate void PowerUpActivated(PowerUpType powerType, PowerUp powerUp);

	[Export]
	public PowerUpType PowerType = PowerUpType.Speed;

	public enum PowerUpType { Speed, Stealth }
	private Sprite _sprite;
	private AnimationPlayer _anim;
	private bool _activated = false;

	private Dictionary<PowerUpType, Rect2> _powerUpRegionRects = new Dictionary<PowerUpType, Rect2>()
	{
		{PowerUpType.Speed, new Rect2(new Vector2(20,20), new Vector2(500,500))},
		{PowerUpType.Stealth, new Rect2(new Vector2(1100,20), new Vector2(500,500))}
	};

	public override void _Ready()
	{
		base._Ready();
		_sprite = GetNode<Sprite>("Sprite");
		_anim = GetNode<AnimationPlayer>("Anim");

		SetType();
	}

	private void SetType()
	{
		_sprite.RegionRect = _powerUpRegionRects[PowerType];
	}
	private void OnPowerUpBodyEntered(PhysicsBody2D body)
	{
		if (_activated)
		{
			return;
		}
		if (body is Parasite p)
		{
			EmitSignal(nameof(PowerUpActivated), PowerType, this);
			_anim.Play("Die");
			_activated = true;
		}
		// Replace with function body.
	}

	private void OnAnimAnimationFinished(String anim_name)
	{
		if (anim_name == "Die")
		{
			QueueFree();
		}
	}


}

