using Godot;
using System;
using System.Collections.Generic;

public class MiniMap : MarginContainer
{
	public NodePath ParasiteNodePath {get; set;} = null;
	
	[Export]
	public float Zoom {get; set;} = 1.5f;

	private TextureRect _grid;
	private PackedScene _parasiteMarkerScn;
	private PackedScene _hostMarkerScn;
	private PackedScene _obstacleMarkerScn;
	private PackedScene _hintNoteMarkerScn;
	private PackedScene _powerUpMarkerScn;
	private Sprite _parasiteMarker;
	private Dictionary<string, PackedScene> icons;

	private Vector2 _gridScale;
	private Dictionary<Node2D, Sprite> _markers = new Dictionary<Node2D, Sprite>();

	public override void _Ready()
	{
		_grid = GetNode<TextureRect>("MarginContainer/Grid");
		_parasiteMarkerScn = (PackedScene) GD.Load("res://Interface/MiniMap/ParasiteMarker.tscn");// GetNode<Sprite>("MarginContainer/Grid/ParasiteMarker");
		_hostMarkerScn = (PackedScene) GD.Load("res://Interface/MiniMap/HostMarker.tscn");// GetNode<Sprite>("MarginContainer/Grid/HostMarker");
		_obstacleMarkerScn = (PackedScene) GD.Load("res://Interface/MiniMap/ObstacleMarker.tscn");//  GetNode<Sprite>("MarginContainer/Grid/ObstacleMarker");
		_hintNoteMarkerScn = (PackedScene) GD.Load("res://Interface/MiniMap/HintNoteMarker.tscn");//  GetNode<Sprite>("MarginContainer/Grid/HintNoteMarker");
		_powerUpMarkerScn = (PackedScene) GD.Load("res://Interface/MiniMap/PowerUpMarker.tscn");// GetNode<Sprite>("MarginContainer/Grid/PowerUpMarker");
		icons = new Dictionary<string, PackedScene>() { 
			{"Obstacle", _obstacleMarkerScn },
			{"Host", _hostMarkerScn },
			{"Parasite", _parasiteMarkerScn },
			{"HintNote", _hintNoteMarkerScn },
			{"PowerUp", _powerUpMarkerScn }
		};
		_parasiteMarker = (Sprite) _parasiteMarkerScn.Instance();
		GetNode("MarginContainer/Grid").AddChild(_parasiteMarker);
		_parasiteMarker.Position = _grid.RectSize / 2f;
		_gridScale = _grid.RectSize / (GetViewportRect().Size * Zoom);

		SetProcess(false);
	}

	public void Init()
	{
		Godot.Collections.Array mapObjects = GetTree().GetNodesInGroup("MiniMapObjects");
		foreach (var item in mapObjects)
		{
			string iconName;
			if (item is StaticBody2D s)
			{
				iconName = "Obstacle";
			}
			else if (item is Parasite p)
			{
				iconName = "Parasite";
			}
			else if (item is Host h)
			{
				iconName = "Host";
			}
			else if (item is HintNote hintNote)
			{
				iconName = "HintNote";
			}
			else
			{
				iconName = "PowerUp";
			}
			Sprite newMarker = (Sprite) icons[iconName].Instance();
			_grid.AddChild(newMarker);
			newMarker.Visible = true;
			_markers[(Node2D)item] = newMarker;
		}
		SetProcess(true);
	}

	public void RemoveMarker(Node2D node)
	{
		_markers[node].QueueFree();
		_markers.Remove(node);
	}

	public override void _Process(float delta)
	{
		if (ParasiteNodePath == null)
		{
			return;
		}
		_parasiteMarker.RotationDegrees = GetNode<Parasite>(ParasiteNodePath).RotationDegreez;// + (float)Math.PI/2f;
		// GD.Print(_parasiteMarker.RotationDegrees);

		foreach (var item in _markers.Keys)
		{
			Vector2 objPos = (item.Position - GetNode<Parasite>(ParasiteNodePath).Position) * _gridScale + _grid.RectSize/2f;
			if (_grid.GetRect().HasPoint(objPos + _grid.RectPosition))
			{
				_markers[item].Scale = new Vector2(.2f,.2f);
			}
			else
			{
				_markers[item].Scale = new Vector2(0.1f, 0.1f);
			}
			objPos.x = Mathf.Clamp(objPos.x, 0, _grid.RectSize.x);
			objPos.y = Mathf.Clamp(objPos.y, 0, _grid.RectSize.y);
			_markers[item].Position = objPos;
		}
	}

	public void SetZoom(float value)
	{
		Zoom = Mathf.Clamp(value, 0.5f, 5f);
		_gridScale = _grid.RectSize / (GetViewportRect().Size * Zoom);

	}
	private void OnMiniMapGUIInput(InputEvent ev)
	{
		if (ev is InputEventMouseButton btn && ev.IsPressed())
		{
			if (btn.ButtonIndex == (int)ButtonList.WheelUp)
			{
				SetZoom(Zoom - 0.1f);
			}
			if (btn.ButtonIndex == (int)ButtonList.WheelDown)
			{
				SetZoom(Zoom + 0.1f);
			}
			
		}
	}

}

